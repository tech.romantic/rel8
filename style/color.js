const blackNight = `rgb(26, 26, 26)`;
const whiteDay = `rgb(249, 249, 249)`;
const trueBlue = `rgb(75, 80, 178)`;
const yellowin = `rgb(234, 236, 94)`;
const hotRed = `rgb(243, 84, 109)`;
const deepPurp = `rgb(109, 80, 178)`;

const nightToDay = {
    0:`#191919`,
    1:`#303030`,
    2:`#474747`,
    3:`#5e5e5e`,
    4:`#757575`,
    5:`#8c8c8c`,	
    6:`#a3a3a3`,
    7:`#bababa`,
    8:`#d1d1d1`,
    9:`#e8e8e8`,
    10:`#ffffff`,
};

// nightShades
const nightShades = {
    0:"#191919",
    1:"#171717",
    2:"#141414",
    3:"#121212",
    4:"#0f0f0f",	
    5:"#0d0d0d",
    6:"#0a0a0a",
    7:"#070707",
    8:"#050505",
    9:"#020202",	
    10:"#000000",
}

// Shades go from 3 lighter, to the color, to 3 darker. Access the original color with [3]
const blueShades = {
    0:'#5d62ba',
    1:'#6f73c1',
    2:'#8185c9',
    3:'#4b50b2',
    4:'#4448a0',
    5:'#3c408e',
    6:'#35387d'
}

export {
    nightToDay,
    blueShades,
    nightShades,
    blackNight,
    whiteDay,
    trueBlue,
    yellowin,
    hotRed,
    deepPurp   
}
