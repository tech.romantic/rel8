package com.rel8;

import android.graphics.Bitmap;
import android.telephony.SmsManager;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.klinker.android.send_message.*;

public class DirectSMSModule extends ReactContextBaseJavaModule {

    public DirectSMSModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    // getName is required to define the name of the module represented in JS
    public String getName() {
        return "DirectSms";
    }

    @ReactMethod
    public void sendDirectSms(String phoneNumber, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNumber, null, msg, null, null);

        } catch (Exception ex) {
            System.out.println("Couldn't send message");
        }
    }

    @ReactMethod
    public void sendDirectMms(String phoneNumber, String msg) {
        Settings settings = new Settings();
        settings.setUseSystemSending(true);
        // first arg of Transaction - should it be current activity or application context?
        Transaction transaction = new Transaction(getCurrentActivity(), settings);
        Message message = new Message(msg, phoneNumber);
        transaction.sendNewMessage(message, Transaction.NO_THREAD_ID);
    }
}
