import { queryRealm } from '../realm/realm';
import { cleanupModal, setSmartTagging, setupModal } from '../redux/app';
import store from '../redux/store';
import ModalRegistrar from './modalService';

export const promptSmartTaggingModal = (numContacts) => {
    ModalRegistrar.register(nextSmartTag);
    store.dispatch(setupModal({
      type: 'confirm',
      title: `Let's tag!`,
      message: `You've got tag suggestions for ${numContacts} contacts, would you like to tag them now?`,
      args: [0]
    }));
}

const nextSmartTag = async (index) => {
    // Grab the right contact in order..
    var contacts = await queryRealm('Contact', 'lastTexted > 0');
    if (index >= contacts.length -1) {
      // Have finished with all contacts - so clear the modal. 
      store.dispatch(setSmartTagging({
        id: false,
        displayName: false,
        smartTags: []
      }));
      store.dispatch(cleanupModal());

    }
    var contactsSorted = contacts.sorted('lastTexted', false);
    var currentContact = contactsSorted[index];
    let { id, displayName, smartTags } = currentContact; 
    let smartTagArr = Array.from(smartTags);

  ModalRegistrar.register(nextSmartTag);
  let nextIndex = index + 1;

  store.dispatch(setupModal({
    type: 'smart',
    title: `${displayName}'s Tags`,
    args: [nextIndex],
    data: {id, displayName, smartTags: smartTagArr}
  }));
}