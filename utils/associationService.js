import Toast from 'react-native-simple-toast';
import { queryRealmFirst, updateObjectInRealm } from '../realm/realm';
import store from '../redux/store';
import { updateTags } from '../redux/tags';
import { retrieveTagListByIds } from './tagsService';

const CONTACTS_PER_TAG_LIMIT = 25; 
// Contacts map & tags map are both objects, with ids as keys and values true if selected. 
export const associateContactsWithTags = (contactsMap, tagsMap) => {
    return new Promise((resolve, reject) => {
        let selectedContactIdList = Object.keys(contactsMap).filter((id) => contactsMap[id]);
        const contactsBeingAssociated = selectedContactIdList.map((contact) => associateContactWithTags(parseInt(contact), Object.values(tagsMap)));
        Promise.all(contactsBeingAssociated).then((updatedContacts) => {
            store.dispatch(updateTags());
            resolve(updatedContacts);
        })
    })
}

// contact by id 
const associateContactWithTags = async (contactId, tags) => {
    return new Promise(async (resolve, reject) => {
        // return the contact result 
        const contactInstance = await queryRealmFirst('Contact', `id == '${contactId}'`);
        if (contactInstance == undefined) {
            Toast.show(`Couldn't tag a contact!`, Toast.LONG);
            return resolve();
        }
        // return tags result. 
        const tagInstances = await retrieveTagListByIds(tags);
        const updatedContact = await linkContactWithTags(contactInstance, tagInstances);
        return resolve(updatedContact);
    });
}

export const linkContactsWithTags = (contacts, tags) => {
    contacts.map((contact) => linkContactWithTag(contact, tags));
    return contacts;
}

export const linkContactWithTags = async (contactResult, tagResults) => {
    // Imposing max of 25 contacts per tag. 
    var tagsUnderContactLimit = tagResults.filter(imposeTagLimit);  
    var newTags = [...contactResult.tags, ...tagsUnderContactLimit];
    var newUniqueTags = [...new Map(newTags.map(item => [item['name'], item])).values()];

    if (tagsUnderContactLimit.length < tagResults.length) {
        let maxedOutTags = tagResults.length - tagsUnderContactLimit.length; 
        Toast.show(`${maxedOutTags} ${(maxedOutTags > 1) ? 'tags have' : 'tag has'} the maximum of 25 contacts - could not add more contacts.`);
    }

    let updatedContact = {
        id: contactResult.id,
        displayName: contactResult.displayName,
        tags: newUniqueTags
    }

    return await updateObjectInRealm('Contact', updatedContact);
}

const imposeTagLimit = (tag) => {
    let currentContactCount = tag.contacts.length; 
    return (currentContactCount <= CONTACTS_PER_TAG_LIMIT);
}


