import { cleanupModal } from "../redux/app";
import store from "../redux/store";

let callback;

function ModalRegistrar () {}

ModalRegistrar.register = function(cb) {
    callback = cb;
};

ModalRegistrar.activate = async function(args) {
    store.dispatch(cleanupModal());
    await callback(...args);
}  

export default ModalRegistrar;
