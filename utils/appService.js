import React from 'react';
import store from '../redux/store';
import { disable } from '../redux/app';
import { updateObjectInRealm } from '../realm/realm';

export const disableApp = async (user) => {
    // Save the user's disabled state in realm
    if (user && !user.disabled) {
        let newUser = {
            id: user.id,
            disabled: true
        }
        // Disabling the user in DB. 
        await updateObjectInRealm('User', newUser);
    }
    // Disable the application 
    store.dispatch(disable());
}
