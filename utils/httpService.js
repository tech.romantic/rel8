
export const uploadToImgur = async (base64) => {
        try {
            let response = await fetch('http://rel8msg.com:3000/image/upload', {
                method: 'POST',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    image: base64
                })
            });
            let json = await response.json();
            return json.imageUrl || false;
        } catch (error) {
            return false; 
        }x
}

export const verifyUser = async (newUser) => {
    try {
        let response = await fetch('http://rel8msg.com:3000/user/verify', {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newUser)
        });
        let result = await response.json();
        return result;
    } catch (error) {
        return false; 
    }
}

export const createUser = async (newUser) => {
    try {
        let response = await fetch('http://rel8msg.com:3000/user/signup', {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newUser)
        });
        let result = await response.json();
        return result;
    } catch (error) {
        return false; 
    }
}

export const sendFeedback = async (user, feedbackObj) => {
    let data = {
        user: user,
        feedback: feedbackObj
    };
    try {
        let response = await fetch('http://rel8msg.com:3000/user/feedback', {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        let res = await response.json();
        return true; 
    } catch (error) {
        return false; 
    }
}

export const contactOptOut = async (user, contact) => {
    let { tags, lastTexted, messageCount } = contact;
    let tagsReadable = tags.map(tag => tag.name).toString(); 
    let disabledMetadata = { tags: tagsReadable, lastTexted, messageCount }; 
    let data = {
        user: user, 
        disabledMetadata
    };

    try {
        let response = await fetch('http://rel8msg.com:3000/user/optout', {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        let res = await response.json();
        return true; 
    } catch (err) {
        return false; 
    }
}

export const alphaCheck = async () => {
    try {
        let response = await fetch('http://rel8msg.com:3000/launch/', {
            method: 'GET',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }
        });
        let res = await response.json();
        return (res.status === 'ALPHA');
    } catch (error) {
        console.log(error);
        return true; 
    }
}

// Push them up, async - if we get a successful response, bulkUpdate them with _pushed: true  
export const sendMessageAnalytics = async (analyticsFlush) => {
    try {
        let response = await fetch('http://rel8msg.com:3000/analytics/', {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(analyticsFlush)
        })
        let res = await response.json();
        return (res && res.status == "success"); 
    } catch (error) {
        return false; 
    }
}