import { keyBy, uniqBy } from 'lodash';
import moment from 'moment';
import phone from 'phone';
import { NativeModules, PermissionsAndroid } from 'react-native';
import { getContactById } from 'react-native-contacts';
import uuid from 'uuid-random';
import { createObjectInRealm, queryRealm, queryRealmFirst, updateObjectInRealm } from '../realm/realm';
import store from '../redux/store';
import { sendMessageAnalytics } from "./httpService";

const DirectSms = NativeModules.DirectSms; 

// An array of tagIDs and a message 
export const sendMessageToTags = async (tags, message, imageLink = null) => {
    return new Promise(async (resolve, reject) => {
        try {
            await getMessagingPermission();
            const tagsToSendTo = await Promise.all(tags.map(async (tagId) => await queryRealmFirst('Tag', `id == '${tagId}'`)));
            const contactIDsToSendTo = uniqBy(tagsToSendTo.reduce((contactIDs, tag) => {
                return (tag.contacts.length > 0) 
                    ? [...contactIDs, ...tag.contacts.map((contactResult) => contactResult.id)]
                    : contactIDs;
            }, []));
            
            // If no contacts are tagged
            if (contactIDsToSendTo.length == 0) resolve({
                status: false, 
                message: 'No contacts for selected tags.'
            })
        
            // TODO - Maybe repair this applicaiton by giving each contact their list of phone numbers so we don't have to do this 
            // native query at the end. Although then we would need a mechanism to repopulate contact data in the application... 

            const nativeContacts = await Promise.all(contactIDsToSendTo
                                            .map(async (contactID) => await waitForNativeContact(contactID)));
             
            const realmContacts = await Promise.all(contactIDsToSendTo.map(async (id) => await queryRealmFirst('Contact', `id == ${id}`)));
            const realmContactsById = keyBy(realmContacts, (contact) => contact.id);
            const allPhoneNumbersStandardizedWithContacts = nativeContacts
                .map((nativeContact) => nativeContact.phoneNumbers.map((nativePhoneNumber) => {return {number: phone(nativePhoneNumber.number)[0], optOutWarning: realmContactsById[nativeContact.recordID].optOutWarning}}))
                .flatMap((arr) => arr);
            const allPhoneNumbersWithOptOutUnique = uniqBy(allPhoneNumbersStandardizedWithContacts, 'number');

            allPhoneNumbersWithOptOutUnique.map((numberMetadata) => {
                let { optOutWarning, number } = numberMetadata;
                let isValidNumber = (number != undefined);
                let messageWithBanner = (message.length < 150 && optOutWarning) ? message.concat(" (rel8sms.com)") : message;
                if (isValidNumber && imageLink != '') DirectSms.sendDirectSms(number, imageLink);
                DirectSms.sendDirectSms(number, messageWithBanner);
            });
            
            let sentToContactIds = contactIDsToSendTo.map((id) => id.toString());
            let sentToTagNames = tagsToSendTo.map((tag) => tag.name);  
            let newMessage = {
                id: uuid(),
                dateSent: moment().format('MMM Do, YYYY h:mm A'),
                text: message,
                sentToContactIds: sentToContactIds,
                sentToTagNames: sentToTagNames,
                _pushed: false  
            };
            if (imageLink) newMessage.imageUrl = imageLink;
            await createObjectInRealm('Message', newMessage);
            flushMessagesToAnalytics();
            await incrementUserMessages();
            await Promise.all(realmContacts.map(updateContact));
            await Promise.all(tagsToSendTo.map(incrementMessagesSentToTags));
            resolve({
                status: true, 
                message: `Successfully sent to ${allPhoneNumbersWithOptOutUnique.length} contacts.`
            });
        } catch (e) {
            console.log(e);
            resolve({
                status: false,
                message: 'Unable to send messages.'
            });
        }
    })
}

const incrementUserMessages = async () => {
    let user = await queryRealmFirst('User');
    await updateObjectInRealm('User', {
        id: user.id,
        messagesSent: user.messagesSent + 1
    });
}
// Fire and forget flush 
const flushMessagesToAnalytics = async () => {
    let state = store.getState();
    let userID = state.app.currentUser.id;
    let messagesToFlush = await queryRealm('Message', '_pushed == false');
    let messageDTOs = messagesToFlush.map((message) => convertToMessageDTO(message, userID));
    let successfulUpload = await sendMessageAnalytics({
        user_id: userID,
        messages: messageDTOs
    });
    if (successfulUpload) {
        await Promise.all(messagesToFlush.map(markMessageAsPushed));
        // These should all be pushed.
    }
}

const markMessageAsPushed = async (message) => {
    let pushedMessage = {
        ...message,
        _pushed: true
    }
    return await updateObjectInRealm('Message', pushedMessage);
}

const convertToMessageDTO = (realmMessage, userID) => {
    let { sentToContactIds, sentToTagNames} = realmMessage;
    return {
        tags: sentToTagNames.map((tag) => tag),
        contactCount: sentToContactIds.length
    }
}

const incrementMessagesSentToTags = async (tag) => {
    await updateObjectInRealm('Tag', {
        id: tag.id,
        messagesSent: tag.messagesSent + 1
    });
}

const updateContact = async (contact) => {
    await updateObjectInRealm('Contact', {
        id: contact.id,
        optOutWarning: true,
        messageCount: contact.messageCount + 1,
        lastTexted: moment.now()
    });
}

// Standardize phone numbers and get a distinct list back (sometimes we have repeated numbers)
export const getDistinctPhoneNumbersForContact = (phoneNumbers) => {
    // TODO: Need to factor in country for international users (second param to phone(num, 'US'))
    const standardizedPhoneNumbers = phoneNumbers.map((number) => phone(number)[0]);
    let distinctNumbers = new Set(); 
    standardizedPhoneNumbers.forEach((number) => {
        distinctNumbers.add(number);
    })
    return Array.from(distinctNumbers);
}

async function waitForNativeContact(contactID) {
    return await getContactByID(contactID);
}

const getContactByID = (contactID) => {
    return new Promise(async (resolve, reject) => {
        getContactById(contactID.toString(), (error, contact) => {
            if(error)console.log(error);
            return resolve(contact);
        });        
    });
}

// Belongs in tag service 
export const getContactsForTag = async (tagId) => {
    let tagResult = await queryRealm('Tag', `id == '${tagId}'`);
    return tagResult[0].contacts.map((contactResult) => contactResult.id);
}

export const getMessagingPermission = () => {
    return new Promise((resolve, reject) => {
        PermissionsAndroid.requestMultiple(
            [PermissionsAndroid.PERMISSIONS.SEND_SMS, PermissionsAndroid.PERMISSIONS.READ_SMS, PermissionsAndroid.PERMISSIONS.RECEIVE_SMS], 
            {
                title: 'Rel8 App SMS Permission',
                message: 'Rel8 needs permission to send text messages for you!',
                buttonNeutral: 'Ask Me Later',
                buttonNegative: 'Cancel',
                buttonPositive: 'OK'
            }
        ).then((status) => {
            var status = Object.keys(status).every((key) => status[key] === 'granted');
            resolve(status);
        })
    })
}

export const sendMessageToNumber = (message, number) => {
    DirectSms.sendDirectSms('6463226277', 'Sent from Rel8');
}