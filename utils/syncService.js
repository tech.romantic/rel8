import SplashScreen from 'react-native-splash-screen';
import { tagSuggestions } from '../data/tagDictionary';
import { AppSchemas, openRealm, queryRealm, queryRealmFirst } from '../realm/realm';
import { endLoading, onboard, startLoading, verify } from '../redux/app';
import store from '../redux/store';
import { disableApp } from './appService';
import { enrichRealmContacts, syncContacts } from './contactsService';
import { verifyUser } from './httpService';
import { promptSmartTaggingModal } from './smartTagService';
import { createOrRetrieveTagListByNames } from './tagsService';
// Look at Async Storage to decide whether we need to pull from native
// contacts storage and persist to Realm. If contacts are stored - then allow
// pages in the application to pull from Realm directly.


// Literally load contacts & tags straight from realm - if they don't exist, then seed. 
export const rel8Startup = async () => {
  SplashScreen.hide();
  store.dispatch(startLoading('Starting up...'));
  // Figure out the user 
  var existingUser = await queryRealmFirst('User');
  // Complete startup user flow 
  if (existingUser && existingUser.verified) store.dispatch(verify());
  if (existingUser && existingUser.onboarded) store.dispatch(onboard());
  if (existingUser) {
    let { email, phone } = existingUser;
    // Alpha check API call 
    let verification = await verifyUser(email, phone);
    if (verification) {
      let { user, verified } = verification;
      let { status } = user;
      let userNotAlpha = (status != 'ALPHA' && status != undefined);
      // Disable the app if needed
      if (userNotAlpha) await disableApp(existingUser);
    }
  }
  
  await enrichContactsFlow();
  store.dispatch(endLoading());
};

export const enrichContactsFlow = async () => {
  // Data
  var contactsSaved = await queryRealm('Contact');
  // Update timestamps and checked for opt out SMS'
  if (contactsSaved && contactsSaved.length > 0) await enrichRealmContacts(contactsSaved);
  store.dispatch(endLoading());
  var contactsWithSmartTags = await queryRealm('Contact', 'hasSmartTags = TRUE');
  if (contactsWithSmartTags.length > 0) promptSmartTaggingModal(contactsWithSmartTags.length);
}

export const loadInitialUserData = async () => {
  await syncContacts();
  await createOrRetrieveTagListByNames(tagSuggestions);
  await enrichContactsFlow();
}

export const deleteAllRealmData = async () => {
  store.dispatch(startLoading('Deleting data...'));
  const realmDB = await openRealm([]);
  realmDB.write(() => {
    AppSchemas.forEach((schema) => {
      realmDB.delete(realmDB.objects(schema.name));  
    });
  })
  store.dispatch(endLoading());
}