import uuid from 'uuid-random';
import { createObjectInRealm, deleteObjectInRealm, queryRealm, queryRealmFirst } from '../realm/realm';
import store from '../redux/store';
import { updateTags } from '../redux/tags';

// TAGS SERVICE
export const createOrRetrieveTagListByNames = async (tags) => {
    // Create the tag list of realm objects
    return await Promise.all(tags.map(queryOrCreateTagByName));
}

export const queryOrCreateTagByName = async (tag) => {
    return new Promise(async (resolve, reject) => {
        // Look for the tag 
        let tagResult = await queryRealm('Tag', `name == '${tag}'`);
        var tagExists = (tagResult.length == 1);
        if (tagExists) {
            return resolve(...tagResult);
        } else {
            var newTag = await createObjectInRealm('Tag',{
                id: uuid(),
                name: tag,
                messagesSent: 0
        });
            return resolve(newTag); 
        }
    });
}

export const retrieveTagListByIds = async (tagIds) => {
    return await Promise.all(tagIds.map(async (id) => await queryRealmFirst('Tag', `id == '${id}'`)));
}

export const createOrRetrieveTagListByIds = async (tags) => {
    // Create the tag list of realm objects
    return await Promise.all(tags.map(queryOrCreateTagById));
}

export const queryOrCreateTagById = async (tag) => {
    return new Promise(async (resolve, reject) => {
        // Look for the tag 
        let tagResult = await queryRealmFirst('Tag', `id == '${tag}'`);
        if (tagResult) {
            return resolve(...tagResult);
        } else {
            var newTag = await createObjectInRealm('Tag',{
                id: uuid(),
                name: tag
            });
            return resolve(newTag); 
        }
    });
}

export const deleteTag = async (tag) => {
    store.dispatch(updateTags());
    await deleteObjectInRealm(tag)
}