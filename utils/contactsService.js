import lda from 'lda';
import { keyBy, sortBy, uniq, uniqBy } from 'lodash';
import moment from 'moment';
import phone from 'phone';
import { DevSettings, PermissionsAndroid } from 'react-native';
import Contacts, { getContactsByPhoneNumber } from 'react-native-contacts';
import SmsAndroid from 'react-native-get-sms-android';
import { openRealm, queryRealm, queryRealmFirst, removeAssociationFromRealm, updateObjectInRealm } from '../realm/realm';
import { startLoading } from '../redux/app';
import store from '../redux/store';
import { getDistinctPhoneNumbersForContact } from './messagingService';

const RECENTLY_TEXTED_CUTOFF_DAYS = 365;

export const syncContacts = async () => {
  await requestContactsPermission();
  var nativeContacts = await getAllContactsPromise();
  var nativeContactsWithInteractions = await enrichNativeContactsWithInteractions(nativeContacts);  
  var contacts = convertNativeContacts(nativeContactsWithInteractions);
  var realmDB = await openRealm([]);
  try {
    var result = await persistAllContacts(contacts, realmDB);
    if (result) {
      return true;
    }
  } catch (e) {
    return false;
    // Do something with error eventually.
  }
};

// Just update the lastTexted timestamp based upon when they were texted 
// and block if you've received a 'STOP' text... 
export const enrichRealmContacts = (realmContacts) => {
  return new Promise(async (resolve, reject) => {
    // Get all text messages since last query 
    let smsMessages = await querySms();
    let smartTagsByPhone = await generateSmartTags(smsMessages);
    let optOutRequests = uniqBy(smsMessages.filter((msg) => msg.body.toUpperCase() == 'STOP'), 'address');
    // Key by phone number with most recent text to get timestamp.  
    let phoneNumbersByTextedTimestamp = keyBy(smsMessages, (sms) => sms.address);

    // Disable all contacts with optOut requests. 
    await Promise.all(optOutRequests.map((num) => {
      return new Promise((resolve, reject) => {
        getContactsByPhoneNumber(phone(num.address)[0], (error, contacts) => {
          if (contacts.length > 0) {
              contacts.forEach(async (contact) => {
                  var disabledContact = {
                    id: parseInt(contact.recordID),
                    blocked: true, 
                    tags: []
                  }
                  updateObjectInRealm('Contact', disabledContact);
                  resolve();
              })
          } else {
            resolve()
          }
        })
      })
    }))

    // Smart tagging objects will contain all recently texted numbers
    // Iterate throuigh its keys, and update the timestamp if the texted timestamp 
    await Promise.all(Object.keys(smartTagsByPhone).map((num) => {
      return new Promise((resolve, reject) => {
        getContactsByPhoneNumber(phone(num)[0], (error, contacts) => {
          if (contacts.length > 0) {
              contacts.forEach(async (contact) => {
                  let contactToUpdate = await queryRealmFirst('Contact', `id == ${contact.recordID}`);
                  let timestamp = (phoneNumbersByTextedTimestamp.hasOwnProperty(num)) ? phoneNumbersByTextedTimestamp[num].date : 0;
                  let enrichedContact = {
                    id: contactToUpdate.id, 
                    lastTexted: timestamp,
                    smartTags: smartTagsByPhone[num],
                    hasSmartTags: true
                  };
                  await updateObjectInRealm('Contact', enrichedContact);
              })
          }
          resolve();
        })
      });
    }))

    await updateOptOutCheck();

    resolve();
  })
}

export const generateSmartTags = async (smsMessages) => {
  store.dispatch(startLoading('Analyzing contacts for tags!'));
  return new Promise(async (resolve, reject) => {
    let smartTagsByNumber = {};
    smsMessages.forEach(async (msg) => {
      let tagModels = lda([msg.body], 2, 1);
      let tagCandidates = uniq(tagModels.flat());
      let contactExists = (smartTagsByNumber.hasOwnProperty(msg.address));
      smartTagsByNumber[msg.address] = (contactExists) ? [...smartTagsByNumber[msg.address],...tagCandidates] : [...tagCandidates];
    });
    // Now take the best tags (10 max?)
    Object.keys(smartTagsByNumber).forEach((num) => {
      let currentTags = smartTagsByNumber[num];
      smartTagsByNumber[num] = uniq(currentTags.sort((tagModelA, tagModelB) => (tagModelA.probability < tagModelB.probability)).slice(0, 10).map(x => x.term));
    })
    resolve(smartTagsByNumber);
  });

}

const updateOptOutCheck = async () => {
    // Now update the last optOut timestamp so we can look at messages from that point onwards next time the app opens 
    let user = await queryRealmFirst('User');
    if (user) {
      return await updateObjectInRealm('User', {
        id: user.id,
        lastOptOutCheck: moment.now()
      });
    }
}

export const loadContactsFromRealm = async () => {
  return await queryRealm('Contact', null, 0, 50);
};

export const queryRealmContacts = async (currentIndex, searchQuery = false) => {
  const realmDB = await openRealm([]);
  let realmContacts = !searchQuery
    ? realmDB.objects('Contact')
    : realmDB
        .objects('Contact')
        .filtered(`displayName BEGINSWITH[c]'${searchQuery}'`);
  var nextIndex = currentIndex + 50; 
  var nextPage = realmContacts.slice(currentIndex, nextIndex);
  var contactsArray = [];
  for (let c in nextPage) {
    contactsArray.push(convertFromRealmContact(nextPage[c]));
  }
  return contactsArray;
};

const convertFromRealmContact = realmObject => {
  const {id, displayName, thumbnailPath} = realmObject;
  var contact = {
    id: parseInt(id),
    displayName,
    thumbnailPath,
  };
  return contact;
};

const persistAllContacts = async (contacts, db) => {
  return new Promise((resolve, reject) => {
    try {
      // Use one transaction
      db.write(async () => {
        // Empty out the DB
        // db.delete(db.objects('Contact'));
        await Promise.all(contacts.map(contact => persistContact(db, contact)));
    });
      // Remember to do this
      db.close();
      resolve(true);
    } catch (e) {
      throw new Error(e);
    }
  });
};

const persistContact = (db, contact) => {
  return new Promise((resolve, reject) => {
    db.create('Contact', contact, 'modified');
  });
};

const requestContactsPermission = () => {
  return new Promise((resolve, reject) => {
    PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.READ_CONTACTS, PermissionsAndroid.PERMISSIONS.SEND_SMS, PermissionsAndroid.PERMISSIONS.READ_SMS, PermissionsAndroid.PERMISSIONS.RECEIVE_SMS], {
      title: 'Requesting Contacts!',
      message: 'rel8 needs contact access to display contacts! No contacts are sent off device.',
      buttonPositive: 'Please accept bare mortal',
    }).then(status => {
      // granted
      var allStatuses = Object.keys(status).every((key) => status[key] === 'granted');
      (allStatuses) ? resolve(true) : DevSettings.reload();
    });
  });
};

const getAllContactsPromise = () => {
  return new Promise((resolve, reject) => {
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        reject(err);
      } else {
        resolve(contacts);
      }
    });
  });
};

const convertNativeContacts = nativeContacts => {
  return nativeContacts.reduce(
    (accumulator, native) => [...accumulator, convertNativeToContact(native)],
    [],
  );
};

const convertNativeToContact = nativeContact => {
  const {recordID, displayName, thumbnailPath, lastTexted } = nativeContact;
  return {
    id: parseInt(recordID),
    displayName: displayName,
    thumbnailPath,
    tags: [],
    lastTexted: (lastTexted) || 0
  };
};

// Mutates native contacts by adding properties if the contact was recently added
// or if the contact was recently texted according to const cutoffs in days. 
const enrichNativeContactsWithInteractions = async (nativeContacts) => {
  return new Promise(async (resolve, reject) => {
    // Most recent first... 
    let smsMessages = await querySms();
    // Reverse this array so we can use _indexBy to get 
    let smsFromOldest = smsMessages.reverse();
    // Key by phone number with most recent text to get timestamp.  
    let phoneNumbersByTextedTimestamp = keyBy(smsFromOldest, (sms) => sms.address);

    for (let index = 0; index < nativeContacts.length; index++) {
      const contact = nativeContacts[nativeContacts.length - (index + 1)];
      let contactTextedTimestamp = wasContactTexted(contact, phoneNumbersByTextedTimestamp); 
      // Add last texted timestamp if the contact was texted.
      if (contactTextedTimestamp) contact.lastTexted = contactTextedTimestamp
    }

    resolve(nativeContacts);
  })
}

const wasContactTexted = (contact, numbersTexted) => {
  // for every contact phone #, does it exist in numberstexted? 
  // if so - return the most timestamp 
  let contactNumbers = getDistinctPhoneNumbersForContact(contact.phoneNumbers.map((nativePhoneNumber) => nativePhoneNumber.number));
  if (contactNumbers.length > 0) {
    let allTextMessagesToContact = contactNumbers.reduce((acc, number) => {
      return (numbersTexted.hasOwnProperty(number)) 
        ? [...acc, numbersTexted[number]]
        : [...acc] 
    }, []);
    let textsSortedByMostRecent = sortBy(allTextMessagesToContact, 'date');
    // Sorts by smallest timestamp to greatest
    // Return the last timestamp... 
    return (textsSortedByMostRecent.length > 0) 
            ? textsSortedByMostRecent[textsSortedByMostRecent.length - 1].date 
            : false; 
  } else {
    return false;
  }

}

const querySms = async () => {
  let user = await queryRealmFirst('User');
  let optOutCheck = (user && user.lastOptOutCheck) ? user.lastOptOutCheck : moment().subtract(RECENTLY_TEXTED_CUTOFF_DAYS, 'days');
  // Query either from 90 days ago or the last time we checked SMS. 

  var smsFilter = {
    box: '', // all inboxes (sent, draft, outbox, failed?)
    minDate: optOutCheck
  }

  return new Promise((resolve, reject) => {
    SmsAndroid.list(
      JSON.stringify(smsFilter),
      (fail) => console.log('Failed getting SMS:', fail),
      (count, smsList) => {
        resolve(JSON.parse(smsList))
      }
    )
  }); 
}

const getSMSForSmartTags = () => {
  var smsFilter = {
    box: '', // all inboxes (sent, draft, outbox, failed?)
    minDate: moment().subtract(RECENTLY_TEXTED_CUTOFF_DAYS, 'days')
  }
  return new Promise((resolve, reject) => {
    SmsAndroid.list(
      JSON.stringify(smsFilter),
      (fail) => console.log('Failed getting SMS:', fail),
      (count, smsList) => {
        resolve(JSON.parse(smsList))
      }
    )
  }); 
}

export const deassociateTagFromContact = async (contactId, tag) => {
  const contactToUpdate = await queryRealmFirst('Contact', `id == '${contactId}'`);
  await removeAssociationFromRealm(contactToUpdate, contactToUpdate.tags, tag, 'name');
}