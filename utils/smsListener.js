import SmsListener from 'react-native-android-sms-listener';
import { getContactsByPhoneNumber } from 'react-native-contacts';
import { updateObjectInRealm } from '../realm/realm';
import { setDisabled } from '../redux/contacts';
import store from '../redux/store';

// If we receive an opt out request while the app is open, disable those contacts. 
export const listenForOptOutMessages = () => {
    SmsListener.addListener(({originatingAddress, body}) => {
        let optOutRequestReceived = (body.toUpperCase() == 'STOP');
        if (optOutRequestReceived) {
            getContactsByPhoneNumber(originatingAddress, (error, contacts) => {
                if (contacts.length > 0) {
                    contacts.forEach(async (contact) => {
                       
                        let contactToDisableID = contact.recordID; 
                        await updateObjectInRealm('Contact', {
                            id: contactToDisableID,
                            blocked: true,
                            tags: []
                        });
                    })
                    // Reload the app to refresh the disabled contacts.
                    store.dispatch(setDisabled());
                }
            })
        }
    })
}
