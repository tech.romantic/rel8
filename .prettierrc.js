module.exports = {
  useTabs: true,
  tabWidth: 4,
  bracketSpacing: false,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  camelCase: false
};
