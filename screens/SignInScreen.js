import * as EmailValidator from 'email-validator';
import phone from 'phone';
import React, { useState } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import PhoneInput from 'react-native-phone-input';
import Toast from 'react-native-simple-toast';
import { connect } from 'react-redux';
import pSBC from 'shade-blend-color';
import styled from 'styled-components';
import img from '../assets/images/rel8retrofinalwhite.png';
import { updateObjectInRealm } from '../realm/realm';
import { setUser, startLoading, verify } from '../redux/app';
import store from '../redux/store';
import { blackNight, deepPurp, trueBlue, whiteDay } from '../style/color';
import { createUser, verifyUser } from '../utils/httpService';
import { loadInitialUserData } from '../utils/syncService';
import { TermsOfService } from './TermsOfService';

const SignInScreen = ({currentUser}) => {

    const [email, setEmail] = useState('');
    const [phoneNumber, setPhone] = useState('+1');
    const [name, setName] = useState('');
    const [tempUser, setTempUser] = useState(false);
    const [isSignup, setIsSignup] = useState(true);

    const toggleSignup = () => {
        setIsSignup(!isSignup);
    }

    const submitEmail = async (email) => {
        
        const validEmail = EmailValidator.validate(email);
        const validPhone = phone(phoneNumber, '', true);
        const validName = (!isSignup || (isSignup && name.length > 2)); 
        if (validName && validEmail && validPhone.length != 0) {
            let userToVerify = {
                email: email, 
                phone: phoneNumber,
                name: (isSignup) ? name : ''
            };

            const result = (isSignup) ? await createUser(userToVerify) : await verifyUser(userToVerify);
                 
            if (result.verified) {
                // Add the default user to realm...
                // Start the app (onboarding flow?)
                let { email, status, user_id, name, messagesSent } = result.user;
                
                let newUser = {
                    id: user_id,
                    name: name,
                    email: email,
                    phone: phoneNumber || '',     
                    verified: true,
                    onboarded: false,
                    disabled: false,
                    messagesSent: messagesSent || 0,
                    lastOptOutCheck: 0
                }
               
                setTempUser(newUser);
            } else {
                Toast.show(result.message, Toast.LONG);
            }
        } else {
            if (!validEmail) {
                Toast.show(`Please enter a valid email`);
            } else if (!validPhone) {
                Toast.show(`Please enter a valid phone`);
            } else if (!validName) {
                Toast.show(`Please enter a valid name`);
            }
        }
    }

    const acceptTos = async () => {
        // Creating the user for the first time (now verified)
        store.dispatch(startLoading('First time setup :)'));
        await updateObjectInRealm('User', tempUser);
        store.dispatch(setUser(tempUser));
        await loadInitialUserData();
        store.dispatch(verify());
    }

    const lightPurp = pSBC(.25, deepPurp);

    return (
         (!tempUser) ? (
            <RootSignIn>
			<LogoContainer>
				<Image
					style={{
						height: 250,
						width: 250,
						backgroundColor: `${blackNight}`,
					}}
					source={img}
				/>
			</LogoContainer>
            <ButtonContainer>
                    <TouchableOpacity
                        onPress={() => toggleSignup()}
                        rippleColor={whiteDay}
                    >

                            <SubmitButton                             
                            style={{
                                borderRadius: 5,
                                color: `${whiteDay}`,
                                borderColor: `${whiteDay}`,
                                borderWidth: 1,
                                width: 100,
                                height: 50,
                            }}>
                                <Text style={{color: `${whiteDay}`}}>{(isSignup) ? "Login Instead" : "Signup Instead"}</Text>
                            </SubmitButton>
                
            
                    </TouchableOpacity>
                </ButtonContainer>
			<EmailContainer>
				<InputContainer>
                    { isSignup && (
                        <LinearGradient
                                colors={[deepPurp, lightPurp]}
                                start={{x: 0.0, y: 1.0}}
                                end={{x: 1.0, y: 0.0}}
                                locations={[0.5, 1.0]}
                                style={{borderRadius: 5, color: `${whiteDay}`, marginBottom: 5}}>
                                <EmailView>
                                    <EmailInput
                                        onChangeText={setName}
                                        value={name}
                                        placeholder="Name"
                                        color={whiteDay}
                                        placeholderTextColor={whiteDay}
                                    />
                                </EmailView>
                        </LinearGradient>
                    )}
					<LinearGradient
						colors={[deepPurp, lightPurp]}
						start={{x: 0.0, y: 1.0}}
						end={{x: 1.0, y: 0.0}}
						locations={[0.5, 1.0]}
						style={{borderRadius: 5, color: `${whiteDay}`}}>
						<EmailView>
							<EmailInput
								onChangeText={setEmail}
								value={email}
								placeholder="Enter email"
								color={whiteDay}
								placeholderTextColor={whiteDay}
							/>
						</EmailView>
					</LinearGradient>
					<LinearGradient
						colors={[deepPurp, lightPurp]}
						start={{x: 0.0, y: 1.0}}
						end={{x: 1.0, y: 0.0}}
						locations={[0.5, 1.0]}
						style={{borderRadius: 5, color: `${whiteDay}`, marginTop: 5}}>
						<EmailView>
							<PhoneInput
                                autoFormat
								onChangePhoneNumber={(text) => setPhone(text)}
								value={phoneNumber}
								placeholder="Enter phone number"
                                textStyle={{
                                    color: whiteDay
                                }}
                                style={{
                                    height: 40,
                                    color: trueBlue
                                }}
                                textProps={{
                                    placeholder: 'Phone number',
                                    placeholderTextColor: whiteDay
                                }}
                                onPressFlag={() => Toast.show('Please enter your number with country code!')}
                     
							/>
						</EmailView>
					</LinearGradient>
                    <View style={{marginTop: 10, marginBottom: 10}}>
                        <InstructionText>
                            { (isSignup) ? 'Welcome to the rel8 alpha.' : ''}
                            
                        </InstructionText>
                        <InstructionText>
                            { (isSignup) ? `Let's get started.` : 'Please enter the email & phone number you signed up with.'}   
     
                        </InstructionText>
                    </View>

					<ButtonContainer>
                        <TouchableOpacity
                            onPress={() => submitEmail(email)}
                            rippleColor={whiteDay}
                        >
                            <LinearGradient
                                colors={[deepPurp, lightPurp]}
                                start={{x: 0.0, y: 1.0}}
                                end={{x: 1.0, y: 0.0}}
                                locations={[0.5, 1.0]}
                                style={{
                                    borderRadius: 5,
                                    color: `${whiteDay}`,
                                    width: 100,
                                    height: 50,
                                }}>
                                <SubmitButton>
                                    <Text>Submit</Text>
                                </SubmitButton>
                            </LinearGradient>
             
                        </TouchableOpacity>
					</ButtonContainer>
				</InputContainer>

			</EmailContainer>
		</RootSignIn>  
        ) : (<TermsOfService accept={acceptTos}></TermsOfService>)

	);
};


const mapStateToProps = (state) => {
    return {
      ...state.app
    }
  }
  
export default connect(mapStateToProps, null)(SignInScreen);
  

const RootSignIn = styled.View`
    flex: 1;
    flexDirection: column;
    backgroundColor: ${blackNight};
`;

const LogoContainer = styled.View`
    flex: 6;
    justifyContent: flex-end;
    alignItems: center;
    backgroundColor: ${blackNight};
`;

const EmailContainer = styled.KeyboardAvoidingView`
    flex: 4; 
    backgroundColor: ${blackNight};
    justifyContent: flex-start;
`;

const EmailView = styled.View`
    backgroundColor: ${blackNight};
    color: ${whiteDay};
    margin: 3px;
`;

const EmailInput = styled.TextInput`
    color: ${whiteDay};
`

const ButtonContainer = styled.View`
  marginTop: 10px;
  marginBottom: 10px; 
  justifyContent: center; 
  alignItems: center;
`;
const SubmitButton = styled.View`
  width: auto;
  height: 50px;
  alignItems: center;
  justifyContent: center; 
  color: ${deepPurp};
`;

const InputContainer = styled.View`
    paddingLeft: 50px;
    paddingRight: 50px;
    marginBottom: 25px;
    color: ${whiteDay};
`;

const InstructionText = styled.Text`
    textAlign: center;
    color: ${whiteDay};
    backgroundColor: ${blackNight};
    flexWrap: wrap;
`


