import React, { useState } from 'react';
import { DevSettings, Linking } from 'react-native';
import { CheckBox } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { Rating } from 'react-native-ratings';
import AwesomeButton from 'react-native-really-awesome-button';
import Toast from 'react-native-simple-toast';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import pSBC from 'shade-blend-color';
import styled from 'styled-components';
import { LargeLeftText } from '../components/textComponents';
import { queryRealm, queryRealmFirst, updateObjectInRealm } from '../realm/realm';
import { endLoading, logout, reOnboard, setupModal, startLoading } from '../redux/app';
import store from '../redux/store';
import { blackNight, deepPurp, hotRed, nightToDay, trueBlue, whiteDay, yellowin } from '../style/color';
import { syncContacts } from '../utils/contactsService';
import { sendFeedback } from '../utils/httpService';
import ModalRegistrar from '../utils/modalService';
import { promptSmartTaggingModal } from '../utils/smartTagService';
import { deleteAllRealmData } from '../utils/syncService';
import { TermsOfService } from './TermsOfService';

const SettingsScreen = ({currentUser}) => {
	const [rating, setRating] = useState(10);
	const [feedback, setFeedback] = useState('');
	const [wouldRecommend, setWouldRecommend] = useState(true);
	const [showTos, setshowTos] = useState(false);
	const lightPurp = pSBC(0.25, deepPurp);

	const submitFeedback = async next => {
		var existingUser = await queryRealmFirst('User');
		let feedbackObject = {
			user: {
				email: existingUser.email,
				id: existingUser.id,
			},
			rating: rating,
			message: feedback,
			wouldPay: wouldRecommend,
		};
		await sendFeedback(currentUser, feedbackObject);
		setRating(5);
		setFeedback('');
		setWouldRecommend(true);
		Toast.show('Thanks for your feedback :)');
		next();
	};

	const resetApp = async () => {
		await deleteAllRealmData();
		DevSettings.reload();
	};

	const logoutUser = async () => {
		let user = await queryRealmFirst('User');
		await updateObjectInRealm('User', {
			id: user.id, 
			verified: false 
		});
		store.dispatch(logout())
	}

	const deleteAppData = async () => {
		ModalRegistrar.register(resetApp);
		store.dispatch(
			setupModal({
				type: 'confirm',
				title: 'Are you sure?!?',
				message: `This will delete all tags, contacts, and messages. We don't save any of these on our servers, you'll have to restart tagging your contacts from the beginning.`,
				args: [],
			}),
		);
	};

	const startSmartTagging = async () => {
		var contactsWithSmartTags = await queryRealm('Contact', 'hasSmartTags = TRUE');
		if (contactsWithSmartTags.length > 0) {
			promptSmartTaggingModal(contactsWithSmartTags.length)
		} else {
			Toast.show('No smart tags available :(');
		}
	};

	const startContactsSync = async () => {
		store.dispatch(startLoading('Resyncing contacts!'));
		await syncContacts();
		store.dispatch(endLoading());
	};

	const openDonationSite = async () => {
		Linking.openURL('https://www.patreon.com/techromantic');
    };
    
	const openOfficialSite = async () => {
		Linking.openURL('http://www.rel8sms.com');
	};

	const openTos = () => {
		return;
	};

	return (
		<SettingsRoot>
			{!showTos ? (
				<>
					<FeedbackHalf>
						<FeedbackContainer>
							<LargeLeftText color={whiteDay}>
								Send Us Feedback!
							</LargeLeftText>
							<MessageContainer>
								<LinearGradient
									colors={[deepPurp, lightPurp]}
									start={{x: 0.0, y: 1.0}}
									end={{x: 1.0, y: 0.0}}
									locations={[0.5, 1.0]}
									style={{
										flex: 3,
										height: 100,
										borderRadius: 5,
										color: `${whiteDay}`,
									}}>
									<EmailView>
										<EmailInput
											onChangeText={setFeedback}
											value={feedback}
											placeholder="How do you like the app?"
											color={whiteDay}
											placeholderTextColor={whiteDay}
											multiline
											numberOfLines={5}
											style={{flex: 1}}
										/>
									</EmailView>
								</LinearGradient>

								<AwesomeButton
									backgroundColor={
										feedback == '' ? whiteDay : hotRed
									}
									textColor={blackNight}
									backgroundDarker={nightToDay['1']}
									backgroundDarker={nightToDay['0']}
									progress
									progressLoadingTime={1000}
									onPress={next => submitFeedback(next)}
									borderRadius={5}
									height={50}
									width={50}
									activityColor={blackNight}
									activeOpacity={0.5}
									springRelease
									style={{
										justifyContent: 'center',
										alignItems: 'center',
										marginLeft: 10,
									}}>
									<MaterialCommunityIcons
										name="email-outline"
										color={
											feedback == ''
												? blackNight
												: whiteDay
										}
										size={20}
										style={{
											height: 'auto',
											flex: 1,
											textAlign: 'center',
										}}
									/>
								</AwesomeButton>
							</MessageContainer>

							<RatingContainer>
								<Rating
									count={5}
									type="heart"
									startingValue={rating}
									size={20}
									ratingColor={yellowin}
									tintColor={blackNight}
									onFinishRating={setRating}
								/>
								<CheckBox
									center
									title="Would you recommend this app?"
									iconRight
									iconType=""
									checkedIcon="thumbs-up"
									uncheckedIcon="thumbs-down"
									checkedColor="green"
									uncheckedColor="red"
									onPress={() =>
										setWouldRecommend(!wouldRecommend)
									}
									progress
									checked={wouldRecommend}
									containerStyle={{
										backgroundColor: `${blackNight}`,
									}}
									textStyle={{
										color: `${whiteDay}`,
									}}
								/>
							</RatingContainer>
						</FeedbackContainer>
					</FeedbackHalf>
					<SettingsHalf>
						<ActionsContainer>
							<AwesomeButton
								backgroundColor={hotRed}
								textColor={whiteDay}
								backgroundDarker={nightToDay['1']}
								backgroundDarker={nightToDay['0']}
								onPress={() => store.dispatch(reOnboard())}
								borderRadius={5}
								width={200}
								height={50}
								activityColor={blackNight}
								activeOpacity={0.5}
								springRelease
								style={{marginBottom: 5}}>
								HELP
							</AwesomeButton>
							<AwesomeButton
								backgroundColor={trueBlue}
								textColor={whiteDay}
								backgroundDarker={nightToDay['1']}
								backgroundDarker={nightToDay['0']}
								onPress={() => openDonationSite()}
								borderRadius={5}
								width={200}
								height={50}
								activityColor={blackNight}
								activeOpacity={0.5}
								springRelease
								style={{marginBottom: 5}}>
								DONATE
							</AwesomeButton>
							<AwesomeButton
								backgroundColor={deepPurp}
								textColor={whiteDay}
								backgroundDarker={nightToDay['1']}
								backgroundDarker={nightToDay['0']}
								onPress={() => startContactsSync()}
								borderRadius={5}
								width={200}
								height={50}
								activityColor={blackNight}
								activeOpacity={0.5}
								springRelease
								style={{marginBottom: 5}}>
								RESYNC CONTACTS
							</AwesomeButton>
							<AwesomeButton
								backgroundColor={whiteDay}
								textColor={blackNight}
								backgroundDarker={nightToDay['1']}
								backgroundDarker={nightToDay['0']}
								onPress={() => logoutUser()}
								borderRadius={5}
								width={200}
								height={50}
								activityColor={blackNight}
								activeOpacity={0.5}
								springRelease
								style={{marginBottom: 5}}>
								LOGOUT
							</AwesomeButton>
							<AwesomeButton
								backgroundColor={whiteDay}
								textColor={blackNight}
								backgroundDarker={nightToDay['1']}
								backgroundDarker={nightToDay['0']}
								onPress={() => startSmartTagging()}
								borderRadius={5}
								width={200}
								height={50}
								activityColor={blackNight}
								activeOpacity={0.5}
								springRelease
								style={{marginBottom: 5}}>
								SMART TAGGING
							</AwesomeButton>
							<AwesomeButton
								backgroundColor={whiteDay}
								textColor={blackNight}
								backgroundDarker={nightToDay['1']}
								backgroundDarker={nightToDay['0']}
								onPress={() => openOfficialSite()}
								borderRadius={5}
								width={200}
								height={50}
								activityColor={blackNight}
								activeOpacity={0.5}
								springRelease
								style={{marginBottom: 5}}>
								rel8 WEBSITE
							</AwesomeButton>
							<AwesomeButton
								backgroundColor={blackNight}
								textColor={whiteDay}
								backgroundDarker={nightToDay['7']}
								onPress={() => setshowTos(true)}
								borderRadius={5}
								width={200}
								height={50}
								activityColor={blackNight}
								activeOpacity={0.5}
								springRelease>
								TERMS & CONDITIONS
							</AwesomeButton>
						</ActionsContainer>
					</SettingsHalf>
				</>
			) : (
				<TermsOfService accept={setshowTos} />
			)}
		</SettingsRoot>
	);
};

const mapState = state => state.app;
export default connect(
	mapState,
	null,
)(SettingsScreen);

const SettingsRoot = styled.SafeAreaView`
    flexDirection: column;
  flex: 1;
  background: ${blackNight};
`;

const FeedbackHalf = styled.KeyboardAvoidingView`
  flex: 2;
  margin: 20px;
`;

const SettingsHalf = styled.View`
    flex: 3;
`;

const FeedbackContainer = styled.View`
    flex: 1;
    padding: 10px;
    borderColor: ${whiteDay};
    borderRadius: 5px;
    borderWidth: 5px;
`;

const SettingsContainer = styled.View`
    flex: 1;
`;

const EmailView = styled.View`
    backgroundColor: ${blackNight};
    color: ${whiteDay};
    margin: 5px;
    flex: 1;
`;

const EmailInput = styled.TextInput`
    color: ${whiteDay};
`

const RatingContainer = styled.View`
    justifyContent: flex-start;
    height: auto;
`;

const MessageContainer = styled.View`
    flexDirection: row;
    width: 100%;
    height: auto; 
    alignItems: center;  
`;

const ActionsContainer = styled.View`
  flex: 1;
  margin: 20px;
  borderColor: ${whiteDay};
  borderRadius: 5px;
  borderWidth: 5px;
  justifyContent: center;
  alignItems: center;
`;