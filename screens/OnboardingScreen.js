import React from 'react';
import { Image } from 'react-native';
import Onboarding from 'react-native-onboarding-swiper';
import { connect } from 'react-redux';
import selectTags from "../assets/images/creating-tags.gif";
import howTo from "../assets/images/onboarding_how_to.png";
import logo from '../assets/images/rel8retrofinalwhite.png';
import messaging from "../assets/images/sending-a-message.gif";
import selectContacts from "../assets/images/tagging-contacts.gif";
import tagsWithFriends from "../assets/images/undraw_friends_with_tags_small.png";
import { updateObjectInRealm } from '../realm/realm';
import { onboard } from '../redux/app';
import store from '../redux/store';
import { blackNight, trueBlue, whiteDay } from '../style/color';
export const OnboardingScreen = ({currentUser}) => {

    const finishOnboarding = async () => {
        await updateObjectInRealm('User', {
            ...currentUser,
            onboarded: true,
            disabled: false
        });
        store.dispatch(onboard())
    }

    return (
        <Onboarding
            showSkip={false}
            onDone={() => finishOnboarding()}
            pages={[
                {
                backgroundColor: blackNight,
                image: <Image style={{
                    height: 250,
                    width: 250,
                    backgroundColor: `${blackNight}`,
                }} source={logo} />,
                title: 'Welcome to rel8!',
                subtitle: `You're about to realize your relationships.`,
                },
                {
                backgroundColor: trueBlue,
                image: <Image style={{
                    resizeMode:'contain',
                    width:400,
                    backgroundColor: `${trueBlue}`,
                }} source={tagsWithFriends} />,
                title: 'How It Works',
                subtitle: `Label your contacts using tags. Send a message to a tag to text those contacts one-on-one.`,
                },
                {
                backgroundColor: blackNight,
                image: <Image style={{
                    resizeMode:'contain',
                    width: 400,
                    height: 500,
                    backgroundColor: `${blackNight}`,
                }} source={selectTags} />,
                title: 'Your Tags',
                subtitle: `Tap tags to select and deselect them. You will see selected tags at the top. Press the plus sign next to your tags to create a custom tag. Tap the top bar to search. Long press a tag to delete it from all contacts.`
                },
                {
                backgroundColor: blackNight,
                image: <Image style={{
                    resizeMode:'contain',
                    width:400,
                    height: 500,
                    backgroundColor: `${blackNight}`,
                }} source={selectContacts} />,
                title: 'Tagging Contacts',
                subtitle: `To tag contacts, check the checkboxes for contacts on the contacts page. Once you have selected your contacts & tags, press the rel8 button to associate them!`,
                },
                {
                backgroundColor: blackNight,
                image: <Image style={{
                    resizeMode:'contain',
                    width:400,
                    height: 500,
                    backgroundColor: `${blackNight}`,
                }} source={messaging} />,
                title: 'Messages Screen',
                subtitle: `To send messages, select tags by tapping on them. Once you have chosen your tags, go to the messaging tab and write a message. You can attach images using the camera icon. Press the send icon to send a message to a contact tagged with any of your selected tags.`,
                },
                {
                backgroundColor: whiteDay,
                image: <Image style={{
                    resizeMode:'contain',
                    width:400,
                    height: 400,
                    backgroundColor: `${whiteDay}`,
                }} source={howTo} />,
                title: 'How To Use Rel8',
                subtitle: `You can use rel8 to improve your professional, social, and personal relationships!
Socially: Keep in touch with old friends, organize new friends, become the leader of your social group.
Personally: Getting help on special topics, favors from friends, knowledge on new happenings.
Professionally: Manage multiple business relationships effectively, communicate quickly, gather intelligence.
`,
                },
            ]}
        >
        </Onboarding>
    )
}

const mapState = state => state.app; 

export default connect(mapState,null)(OnboardingScreen)
