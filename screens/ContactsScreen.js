import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import store from '../redux/store';
import { Autocomplete } from 'react-native-autocomplete-input';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  FlatList,
  View,
  Text,
  StatusBar,
  PermissionsAndroid,
} from 'react-native';
import { loadContactsFromRealm } from '../utils/contactsService';
import ContactsList from '../components/contentComponents/contactsList';
import {ContactsControls} from '../components/contentComponents/contactsControls';
import { blackNight } from '../style/color';
import LinkButton from '../components/contentComponents/linkButton';
const ContactsScreen = () => {
  return (
    <>
    <SafeAreaView
      style={{
        backgroundColor: `${blackNight}`,
        flex: 1
      }}
      >
      <ContactsControls></ContactsControls>
      <ContactsList></ContactsList>
    </SafeAreaView>
    <LinkButton/>
    </>
  );
}



const mapState = state => {
  return {
    ...state.contacts,
  };
};

// export default ContactsScreen;
export default connect(
  mapState,
  null,
)(ContactsScreen);
