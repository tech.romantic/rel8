import React from 'react';
import styled from 'styled-components';
import {
    View, 
    Text, 
    ScrollView,
    Dimensions
} from 'react-native';
import {
    LargeLeftText
} from '../components/textComponents';
import { whiteDay, blackNight, trueBlue } from '../style/color';
import HTML from 'react-native-render-html';
import { tos } from '../assets/content/tos';
import AwesomeButton from "react-native-really-awesome-button";
export const TermsOfService = ({accept}) => {
    return (
        <ToSRoot> 
            <LargeLeftText color={blackNight}>
            rel8 Terms of Service &
            </LargeLeftText>
            <LargeLeftText color={blackNight}>
            End User License Agreement
            </LargeLeftText> 
            <ScrollView  >
                <HTML html={tos} />
                <ButtonCenter>
                    <AwesomeButton
                                backgroundColor={trueBlue}
                                textColor={whiteDay}
                                onPress={() => accept()}
                                borderRadius={5}
                                height={50} 
                                width={200}   
                                activityColor={blackNight}
                                activeOpacity={0.5}
                                springRelease
                                style={{justifyContent: 'center', alignItems: 'center', marginLeft: 10}}
                            
                            >
                            I Accept
                    </AwesomeButton>
                </ButtonCenter>
            </ScrollView>
        </ToSRoot>
    )
}

const ToSRoot = styled.View`
  flex: 1;
  padding: 20px;
  backgroundColor: ${whiteDay};
`;

const ButtonCenter = styled.View`
  flex: 1;
  justifyContent: center;
  alignItems: center;
`;
