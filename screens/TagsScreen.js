import React, { useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import FavoriteTags from '../components/contentComponents/favoriteTagsList';
import LinkButton from '../components/contentComponents/linkButton';
import TagMultiSelect from '../components/contentComponents/tagMultiSelect';
import YourTagsList from '../components/contentComponents/yourTagsList';
import { blackNight } from '../style/color';
const TagsScreen = ({suggestedTags, tagsUpdated}) => {
  const [tagsList, setTagsList] = useState([]);

  return (
    <>
      <TagsScreenRoot>
        <TagMultiSelect tags={tagsList}></TagMultiSelect>
        <TagUIContainer>
          <FavoriteTags></FavoriteTags>
          <YourTagsList></YourTagsList>
        </TagUIContainer>
      </TagsScreenRoot>
      <LinkButton/>
    </>
  );

}

const TagsScreenRoot = styled.View`
  flex: 1;
  backgroundColor: ${blackNight};
`

const TagUIContainer = styled.View`
  paddingLeft: 10px;
  paddingRight: 10px;
  flex: 2;
`
const mapState = state => {
  return {
    ...state.tags,
  };
};

export default connect(
  mapState,
  null,
)(TagsScreen);
