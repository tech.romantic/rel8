import React, { useState } from 'react';
import { Image, StyleSheet, TextInput } from 'react-native';
import { ActivityIndicator, Caption } from 'react-native-paper';
import PhotoUpload from 'react-native-photo-upload';
import Toast from 'react-native-simple-toast';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { MessagesList } from '../components/contentComponents/messagesList';
import TagMultiSelect from '../components/contentComponents/tagMultiSelect';
import { MessagingView } from '../components/layoutComponents';
import { blackNight, deepPurp, hotRed, whiteDay } from '../style/color';
import { uploadToImgur } from '../utils/httpService';
import { sendMessageToTags } from '../utils/messagingService';

const MessagingScreen = ({ selectedTags, version }) => {
    const [message, setMessage] = useState('');
    const [image, setImage] = useState(null);
    const [messageRefresh, setMessageRefresh] = useState(false);
    const [sending, setSending] = useState(false);
    let tagsSelected = (selectedTags.length != 0);
    let validMessage = (message.length != '');
    const sendIsDisabled = (!tagsSelected || !validMessage);


    const sendMessage = async () => {
      if(!sendIsDisabled) {
        setSending(true);
        let imageLink = null;
        if (image) {
          imageLink = await uploadToImgur(image);
          if(!imageLink) {
            Toast.show(`Couldn't upload image!`);
            setImage(null);
          }
        } 
        let sentStatus = await sendMessageToTags(selectedTags, message, imageLink);
        setMessage('');
        setImage(null);
        setSending(false);
        setMessageRefresh(!messageRefresh);
        Toast.show(sentStatus.message, Toast.LONG);
      }

    }

    const convertImage = async (base64) => {
      if(base64) {
        setImage(base64);
      }
    }

    return (
		<MessagingView>
			<MessageHistoryContainer>
				<MessagesList messageRefresh={messageRefresh} />
			</MessageHistoryContainer>

			<MessageControlsContainer>
				<TagMultiSelect displayOnly />
				<MessageFabRow>
					<TextInput
            selectTextOnFocus={true}
						style={styles.textareaContainer}
						onChangeText={setMessage}
						defaultValue={message}
						multiline
						maxLength={160}
						placeholder="Write your message here!"
						placeholderTextColor={'#c7c7c7'}
            underlineColorAndroid={'transparent'}
            maxLength={160}
					/>
					<MessageActions>
						<PhotoUpload
							onPhotoSelect={convertImage}
							color={blackNight}
							style={{width: 25}}>
							{image && image.length > 0 ? (
								<Image
									style={{
										width: 40,
										height: 40,
										borderRadius: 20,
									}}
									source={{
										uri: `data:image/gif;base64,${image}`,
									}}
								/>
							) : (
								<MaterialCommunityIcons
									name="camera"
									size={30}
									color={whiteDay}
									style={{height: 'auto'}}
								/>
							)}
						</PhotoUpload>
						{sending ? (
							<ActivityIndicator
								animated={true}
								color={whiteDay}
							/>
						) : (
							<FontAwesome5
								name="send"
								onPress={sendMessage}
								disabled={sendIsDisabled}
								color={sendIsDisabled ? hotRed : whiteDay}
								size={25}
								style={{height: 'auto'}}
							/>
						)}
					</MessageActions>
				</MessageFabRow>
			</MessageControlsContainer>
		</MessagingView>
	);
}

const mapStateToProps = (state) => {
  return {
    selectedTags: state.tags.selectedTags,
    version: state.setting.version,
  }
}

export default connect(mapStateToProps, null)(MessagingScreen);

const MessageStatusLabel = ({status}) => {
  return (status.message != '') 
    ? <Caption style={{color: (!status.status) ? 'red' : whiteDay}}>{status.message}</Caption> 
    : null;
}

const MessageHistoryContainer = styled.View`
  flex: 9;
  padding: 10px;
  backgroundColor: ${blackNight};
`

const MessageControlsContainer = styled.KeyboardAvoidingView`
  height: 100px;
  width: 100%;
`;

const MessageActions = styled.View`
  width: 100px;
  paddingRight: 20px;
  flexDirection: row;
  alignItems: center;

`

const MessageFabRow = styled.KeyboardAvoidingView`
  height: 50px;   
  flex: 1;
  flexDirection: row; 
  background: ${deepPurp};
`

const styles = StyleSheet.create({
  textareaContainer: {
    flex: 1,
    height: '100%',
    backgroundColor: '#F5FCFF',
    borderTopRightRadius: 50,
    borderBottomRightRadius: 50,
  },
  textarea: {
    textAlignVertical: 'top',  // hack android
    fontSize: 16,
    color: `${blackNight}`,
    padding: 5
  },
});

