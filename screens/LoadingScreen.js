import React from 'react';
import styled from 'styled-components';
import {ActivityIndicator, Colors} from 'react-native-paper';
import { CenteredView } from '../components/layoutComponents';
import { SpacedText } from '../components/textComponents';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { blackNight } from '../style/color';
import AnimatedLoader from "react-native-animated-loader";
import figure from '../assets/animations/18337-infinity-loader-concept.json';

export default LoadingScreen = ({message}) => (
  <Background>
    <CenteredView>
      <AnimatedLoader 
        visible={true}
        source={require("../assets/animations/18337-infinity-loader-concept.json")}
        animationStyle={{
          width: 500,
          height: 500
        }}
        speed={0.75}
      />
      <SpacedText>{message}</SpacedText>
    </CenteredView>
  </Background>
);

const Background = styled.SafeAreaView`
  background: ${blackNight};
  flex: 1;
`;

