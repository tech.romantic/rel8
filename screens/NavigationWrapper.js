import React, {useState, useEffect} from 'react';
import {NavigationContainer, useRoute} from '@react-navigation/native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import ContactsScreen from './ContactsScreen';
import LoadingScreen from './LoadingScreen';
import TagsScreen from './TagsScreen';
import MessagingScreen from './MessagingScreen';
import Ionicon from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome';
import AntDesign  from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect} from 'react-redux';
import {ActivityIndicator, Colors, Button} from 'react-native-paper';
import LinkButton from '../components/contentComponents/linkButton';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Modal
} from 'react-native';
import { trueBlue, hotRed, whiteDay, deepPurp, blackNight } from '../style/color';
import ModalRegistrar from '../utils/modalService';
import store from '../redux/store';
import { cleanupModal } from '../redux/app';
import { ModalSwitcher } from './modals';
import SignInScreen from './SignInScreen';
import DisabledScreen from './DisabledScreen';
import OnboardingScreen from './OnboardingScreen';
import SettingsScreen from './SettingsScreen';
import { MenuProvider } from 'react-native-popup-menu';

export const Tab = createMaterialBottomTabNavigator();

const NavgiationWrapper = (props) => {

  return (
    <MenuProvider>
      <NavigationContainer>
        <FlowDirector {...props}></FlowDirector>      
      </NavigationContainer>
    </MenuProvider>
  );
};

const FlowDirector = ({loading, loadingMessage, modal, currentUser, disabled}) => {
  let { verified, onboarded } = currentUser;
  let currentStates = [loading, disabled, !verified, !onboarded, true];
  let flows = {
    loading: (<LoadingScreen message={loadingMessage}/>),
    isDisabled: (<DisabledScreen></DisabledScreen>),
    isVerified: (<SignInScreen></SignInScreen>),
    isOnboarded: (<OnboardingScreen></OnboardingScreen>),
    app: (<>
      <TabNavigationAndScreens/>
      <ModalSwitcher modal={modal}/>
    </>)
  };

  let components = Object.values(flows);
  let componentToRender = components[currentStates.findIndex((state) => state)];
  return componentToRender;
}

const TabNavigationAndScreens = () => (
  <Tab.Navigator
    barStyle={{backgroundColor: `${trueBlue}`}}
    labeled={true}
    >
    <Tab.Screen name="Tags" component={TagsScreen} 
       options={{
         tabBarLabel: 'Tags',
         tabBarIcon: ({color}) => (
           <FontAwesome5 name="hashtag" size={25} color={whiteDay}/>
         ),
         tabBarColor: trueBlue
       }}

    />
    <Tab.Screen name="Contacts" component={ContactsScreen} 
       options={{
         tabBarLabel: 'Contacts',
         tabBarIcon: ({color}) => (
           <Ionicon name="md-contact" size={25} color={whiteDay}></Ionicon>
         ),
         tabBarColor: hotRed
       }}
    />
    <Tab.Screen name="Message" component={MessagingScreen} 
       options={{
         tabBarLabel: 'Messaging',
         tabBarIcon: ({color}) => (
           <MaterialCommunityIcons name="message-plus" size={25} color={whiteDay}></MaterialCommunityIcons>
         ), 
         tabBarColor: deepPurp
       }}
    />
    <Tab.Screen name="Settings" component={SettingsScreen} 
       options={{
         tabBarLabel: 'Settings',
         tabBarIcon: ({color}) => (
           <MaterialCommunityIcons name="settings" size={25} color={whiteDay}></MaterialCommunityIcons>
         ), 
         tabBarColor: blackNight
       }}
    />
  </Tab.Navigator>
)

const mapState = state => state.app;

export default connect(
  mapState,
  null,
)(NavgiationWrapper);
