import { uniq } from 'lodash';
import React, { useState } from 'react';
import { Modal } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import { Divider } from 'react-native-paper';
import styled from 'styled-components';
import { DisplayTag, SmartTag } from '../components/contentComponents/tag';
import YourTagsList from '../components/contentComponents/yourTagsList';
import { LargeLeftText, LargeText } from '../components/textComponents';
import { queryRealmFirst, updateObjectInRealm } from '../realm/realm';
import { cleanupModal } from '../redux/app';
import store from '../redux/store';
import { hotRed, nightToDay, trueBlue, whiteDay } from '../style/color';
import { linkContactWithTags } from '../utils/associationService';
import ModalRegistrar from '../utils/modalService';
import { createOrRetrieveTagListByNames } from '../utils/tagsService';

export const ModalSwitcher = ({modal}) => {

    let modalToRender;
    switch (modal.type) {
        case 'confirm':
            modalToRender = (<ConfirmContent modal={modal}/>);
            break;
        case 'image':
            modalToRender = (<ImageModal modal={modal}/>);
            break;
        case 'smart':
            modalToRender = (<SmartModal modal={modal} />);
            break;
    }

    return (
        <Modal
            animationType="slide"
            animationOut={(modal.type == 'confirm') && 'bounceOutDown'}
            visible={modal.visible}
            statusBarTranslucent={true}
            style={{
                justifyContent: 'flex-end',
                margin: 0,
                backgroundColor: (modal.type != 'image' && modal.type != 'smart') && 'transparent'
            }}
            onDismiss={() => store.dispatch(cleanupModal())}
            onBackdropPress={() => store.dispatch(cleanupModal())}
            transparent={(modal.type != 'image')}
        >
            {modalToRender}
        </Modal>
    )
}

const SmartModal = ({modal}) => {

    let { title, data, args } = modal;
    let {  id, displayName, smartTags } = data; 
    const [tagsToApply, setTagsToApply] = useState([])

    const addTag = (tag) => {
        setTagsToApply(uniq([...tagsToApply, tag]));
    }

    const removeTag = (tag) => {
        setTagsToApply(tagsToApply.filter((smartTag) => (smartTag != tag)));
    }

    const submitTags = async () => {
        let contactToAssignTo = await queryRealmFirst('Contact', `id == '${id}'`);
        let realmTags = await createOrRetrieveTagListByNames(tagsToApply);
        await linkContactWithTags(contactToAssignTo, realmTags);
        // Delete the list of smart tags
        await updateObjectInRealm('Contact', {
            id: contactToAssignTo.id,
            smartTags: [],
            hasSmartTags: false
        });

        store.dispatch(cleanupModal())
        ModalRegistrar.activate(args);
    }
    return (
        <FullScreenModal>
            <LargeText>{title}</LargeText>
            <ConfirmText>Tap on tags to add them to the contact. Press the plus sign for a custom tag. Press next to move on or quit to finish.</ConfirmText>
            <LargeLeftText color={whiteDay}>New Tags</LargeLeftText>
            <TagsWrapper>
                {(tagsToApply.length == 0) 
                    ? <ConfirmText>No tags selected!</ConfirmText>
                    :  tagsToApply.map((tag, index) => (
                        <DisplayTag
                            tag={tag}
                            key={index}
                            deselect={removeTag}
                        />
                    ))
                }

            </TagsWrapper>
            <LargeLeftText color={whiteDay}>Suggested Tags</LargeLeftText>
            <TagsWrapper>
                {(smartTags.length == 0) 
                    ? <ConfirmText>No tag suggestions :(</ConfirmText>
                    :  smartTags.map((tag, index) => (
                        <SmartTag
                            tag={tag}
                            key={index}
                            select={addTag}
                        />
                    ))
                }

            </TagsWrapper>
            <YourTagsList select={addTag}></YourTagsList>
            <ButtonBar>  
                <ButtonContainer>
                    <ModalButton color={hotRed} title="Quit" onPress={() => store.dispatch(cleanupModal())}>
                        Confirm
                    </ModalButton>                  
                </ButtonContainer>
                <ButtonContainer>
                    <ModalButton color={trueBlue} title="Next"  onPress={() => submitTags()}>
                    </ModalButton>
                </ButtonContainer>

              </ButtonBar>
        </FullScreenModal>
    )
}

const TagsWrapper = styled.View`
  backgroundColor: ${nightToDay["0"]};
  borderWidth: 1px;
  borderColor: ${nightToDay["2"]};
  paddingBottom: 10px;
  padding: 5px;
  borderRadius: 10px;
  flexDirection: row;
  height: auto; 
  width: 100%;
  alignItems: flex-start;
  flexWrap: wrap; 
`

  
const ImageModal = ({modal}) => {
    let imageViewerArr = modal.args.map((imageUrl) => {
        return {
            url: imageUrl
        }
    });
    return (
        <ImageViewer 
            imageUrls={imageViewerArr}
            enableImageZoom
            enableSwipeDown
            saveToLocalByLongPress    
            onSwipeDown={() => store.dispatch(cleanupModal())}
            renderFooter={() => (<ImageModalFooter></ImageModalFooter>)}
        />
    )
}

const ConfirmContent = ({modal}) => {
    let {title, message, args} = modal; 
    return (
          <ModalRoot>
              <TitleContainer>
                <LargeText size={24} color={whiteDay}>{title}</LargeText>
              </TitleContainer>   
              <Divider color={nightToDay["7"]}/> 
              <ConfirmText>{message}</ConfirmText>
              <ButtonBar>  
                <ButtonContainer>
                    <ModalButton color={trueBlue}   title="ok"  onPress={() => ModalRegistrar.activate(args)}>
                    </ModalButton>
                </ButtonContainer>
                <ButtonContainer>
                    <ModalButton color={hotRed} title="Cancel" onPress={() => store.dispatch(cleanupModal())}>
                        Confirm
                    </ModalButton>                  
                </ButtonContainer>
              </ButtonBar> 
          </ModalRoot>
    );
}

export const ImageModalFooter = () => {
    return (
        <FooterContainer>
            <FooterText>Pinch to zoom, swipe down to close.</FooterText>
        </FooterContainer>
    )
}

const FooterContainer = styled.View`
  marginLeft: 25px;  
  height: 50px;
  justifyContent: center;
  alignItems: center;
  color: ${whiteDay};
  marginBottom: 20px;
`;

const FooterText = styled.Text`
    width: 100%;
    color: ${whiteDay};
    fontSize: 16px;
    textAlign: center; 
`
export const ModalRoot = styled.View`
    height: auto;
    backgroundColor: ${nightToDay["1"]};
    padding: 20px;
    paddingTop: 20px;
    position: absolute;
    bottom: 0;
    alignSelf: center;
    width: 90%;
    borderTopStartRadius: 50px;
    borderTopEndRadius: 50px;
    elevation: 5;
`;

export const FullScreenModal = styled.View`
    flex: 1;
    backgroundColor: ${nightToDay["1"]};
    padding: 20px;
    paddingTop: 20px;
    position: absolute;
    bottom: 0;
    alignSelf: center;
`;
  
export const ButtonBar = styled.View`
    height: auto;
    flex: 1;
    width: 100%;
    flexDirection: row;
`;

const ButtonContainer = styled.View`
    flex: 1;
    padding: 10px;
`;

const TitleContainer = styled.View`  
    marginBottom: 10px;
    flex: 1;
`
const ModalButton = styled.Button`
  alignSelf: stretch;
  backgroundColor: ${(props) => props.color};
  color: ${whiteDay};
  alignItems: center; 
  fontSize: 16px;
  fontWeight: bold;
  marginTop: 5px;
  marginTop: 5px;
`;

export const ConfirmText = styled.Text`
    color: ${nightToDay["8"]};
    textAlign: center;
    flexWrap: wrap;
    fontSize: 16px;
    paddingTop: 10px;
    paddingBottom: 10px;
    paddingLeft: 5px;
`;
