import React, {useState} from 'react';
import { SpacedText, LargeText } from '../components/textComponents';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  TouchableOpacity
} from 'react-native';
import styled from 'styled-components';
import img from '../assets/images/rel8retrofinalwhite.png';
import { blackNight, deepPurp, whiteDay } from '../style/color';
import pSBC from 'shade-blend-color';
import LinearGradient from 'react-native-linear-gradient';

export default DisabledScreen = () => {

    const lightPurp = pSBC(.25, deepPurp);
    return (
		<RootSignIn>
			<LogoContainer>
				<Image
					style={{
						height: 250,
						width: 250,
						backgroundColor: `${blackNight}`,
					}}
					source={img}
				/>
			</LogoContainer>
			<EmailContainer>
				<InputContainer>
					<LinearGradient
						colors={[deepPurp, lightPurp]}
						start={{x: 0.0, y: 1.0}}
						end={{x: 1.0, y: 0.0}}
						locations={[0.5, 1.0]}
						style={{borderRadius: 5, color: `${whiteDay}`}}>
						<EmailView>
							<LargeText>
                                The rel8 alpha period is over. 
                            </LargeText>
                            <InstructionText>Has it been that long already? </InstructionText>
                            <InstructionText>Thanks so much for being one of the first users! </InstructionText>
                            <InstructionText>Let's get back to building relationships.</InstructionText>
                            <InstructionText>Head over to rel8msg.com for the new update.</InstructionText>
                            <InstructionText>You'll still have all your data, just with more features.</InstructionText>
                            <InstructionText>And don't worry - there's a free version.</InstructionText>
						</EmailView>
					</LinearGradient>
				</InputContainer>

			</EmailContainer>
		</RootSignIn>
	);
};

const RootSignIn = styled.View`
    flex: 1;
    flexDirection: column;
    backgroundColor: ${blackNight};
`;

const LogoContainer = styled.View`
    flex: 6;
    justifyContent: flex-end;
    alignItems: center;
    backgroundColor: ${blackNight};
`;

const EmailContainer = styled.KeyboardAvoidingView`
    flex: 4;
    paddingTop: 25px; 
    backgroundColor: ${blackNight};
    justifyContent: flex-start;
`;

const EmailView = styled.View`
    backgroundColor: ${blackNight};
    color: ${whiteDay};
    margin: 3px;
`;

const EmailInput = styled.TextInput`
    color: ${whiteDay};
`

const InputContainer = styled.View`
    marginBottom: 25px;
    color: ${whiteDay};
`;

const InstructionText = styled.Text`
    marginTop: 10px;
    marginBottom: 10px;
    textAlign: center;
    color: ${whiteDay};
    backgroundColor: ${blackNight};
    flexWrap: wrap;
`


