import React from 'react';
import styled from 'styled-components';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    KeyboardAvoidingView
  } from 'react-native';

const MessagingView = styled.SafeAreaView`
  flex: 1;
  flexDirection: column; 
  width: 100%;
`

const CenteredView = styled.View`
  flexGrow: 1;
  alignItems: center;
  justifyContent: center;
`;

const HorizontalSplitRow = styled.ScrollView`
  height: 50px;
  paddingLeft: 10px;
  paddingRight: 10px;
  borderBottomWidth: 1px;
  borderStyle: solid;
  borderColor: grey;
  backgroundColor: ${props => (props.selected ? "grey" : "white")};
  flexDirection: row;
`;

const CenteredRow = styled.View`
  paddingLeft: 10px;
  paddingRight: 10px;
  justifyContent: center;
  alignItems: center;
  flexDirection: row;
`


export {CenteredView, HorizontalSplitRow, CenteredRow, MessagingView};
