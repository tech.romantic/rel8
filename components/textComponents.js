import React from 'react';
import styled from 'styled-components';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { whiteDay } from '../style/color';

const LargeText = styled.Text`
  fontFamily: Indigo Regular;
  width: auto;
  fontSize: ${(props) => props.size || '24'}px;
  fontWeight: bold;
  textAlign: center;
  color: ${(props) => props.color || whiteDay};
`;

const LargeLeftText = styled.Text`
  fontFamily: Indigo Regular;
  fontSize: ${(props) => props.size || '24'}px;
  fontWeight: bold;
  textAlign: left;
  color: ${(props) => props.color};
  marginBottom: 5px;
`;

const SpacedText = styled.Text`
  fontSize: 24px; 
  color: ${whiteDay};
  marginTop: 100px;
  padding: 10px;
`;

export {LargeText, LargeLeftText, SpacedText};
