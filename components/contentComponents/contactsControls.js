import React, {useState, useEffect} from 'react';
import styled from 'styled-components';
import ContactsSearch from './contactsSearch';
import {View} from 'react-native';
import TagMultiSelect from './tagMultiSelect';

export const ContactsControls = () => {
  return (
    <ControlsContainer>
      <TagMultiSelect relative={true}/>
      <ContactsSearch />
    </ControlsContainer>

  );
};

const ControlsContainer = styled.View`
    height: auto;
    backgroundColor: white;
`
