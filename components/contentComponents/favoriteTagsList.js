import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { queryRealm } from '../../realm/realm';
import { blackNight, whiteDay } from '../../style/color';
import { LargeLeftText } from '../textComponents';
import { FavoriteTag } from './favoriteTag';

const FavoriteTags = ({tagsUpdated}) => {
  let [tagsList,setTagsList] = useState([]);
  useEffect(() => { 
    async function populateTags() {
        var tagsWithContactsOrMessages = await queryRealm('Tag', 'contacts.@count > 0 OR messagesSent > 0');
        var favoriteTags = tagsWithContactsOrMessages.sorted('messagesSent', true);
        let defaultFavs = await queryRealm('Tag', '');
        let defaultFavsMinusNormalFavs = defaultFavs.filter((tag) => favoriteTags.find((favTag) => (favTag.name != tag.name)));
        (favoriteTags.length > 0) 
          ? setTagsList([...favoriteTags, ...defaultFavsMinusNormalFavs].slice(0,6))
          : setTagsList(defaultFavs.slice(0,6));
    }
    populateTags();
  }, [tagsUpdated]);

  return (
    <FavoriteTagsRoot> 
     <LargeLeftText size={24} color={whiteDay}>Favorite Tags</LargeLeftText>
      <TagsFlatList
        extraData={tagsUpdated}
        data={tagsList}
        renderItem={({item, index}) => <FavoriteTag tag={item} index={index}/>}
        numColumns={2}
        keyExtractor={(item, index) => index.toString()}
      />
    </FavoriteTagsRoot>
  );
};

const FavoriteTagsRoot = styled.View`
  height: auto;
  paddingLeft: 5px;
  paddingRight: 5px;
  paddingTop: 5px;
`
const TagsFlatList = styled.FlatList`
  backgroundColor: ${blackNight};
`

const mapStateToProps = (state) => {
  return {
    ...state.tags
  }
}

export default connect(mapStateToProps, null)(FavoriteTags);

