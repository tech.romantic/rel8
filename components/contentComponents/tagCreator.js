import React, {useState, useEffect} from 'react';
import styled from 'styled-components';
import {TextInput, FAB} from 'react-native-paper';
import store from '../../redux/store';
import { selectTag } from '../../redux/tags';
// const searchUpdate = (searchQuery) => {
//   store.dispatch(setSearchQuery(searchQuery));
// }

// const debouncedSearchUpdate = debounce(searchUpdate, 500);

export const TagCreator = ({search, set}) => {

  const [newTag, setNewTag] = useState('');

  const addCustomTag = () => {
    if (newTag != '') {
      store.dispatch(selectTag(newTag));
      setNewTag('');
    }
  }

  return (
    <CreatorContainer>
      <TextInput
        mode="flat"
        label="Create a tag or select one below..."
        onChangeText={setNewTag}
        value={newTag}
        style={{flex: 1}}
      />
      <TagSubmitContainer>
        <FAB
          small
          icon="plus"
          onPress={addCustomTag}
        />
      </TagSubmitContainer>
    </CreatorContainer>
  );
};


const CreatorContainer = styled.View`
  flexDirection: row;
`

const TagSubmitContainer = styled.View`
  justifyContent: center;
  alignItems: center; 
  flex: 1; 
  position: absolute;
  left: 80%;
  width: 20%;
  height: 100%;

`