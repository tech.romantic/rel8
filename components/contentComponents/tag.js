import randomColor from 'randomcolor';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import styled from 'styled-components';
import { setupModal } from '../../redux/app';
import store from '../../redux/store';
import { selectTag } from '../../redux/tags';
import ModalRegistrar from '../../utils/modalService';
import { deleteTag } from '../../utils/tagsService';

export const getColor = (name) => randomColor({
  seed: name,
  luminosity: 'light'
});

export const getDarkColor = (name) => randomColor({
  seed: name,
  luminosity: 'dark'
});

export const AttachedTag = ({name, index, onLongPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={(onLongPress) && (() => onLongPress(name))}
      style={{ height: 'auto'}}
    >
        <RoundedTag color={getColor(name)} key={index} width='auto'>
          <ContactTagText >{name}</ContactTagText>
        </RoundedTag>    
    </TouchableOpacity>
  )
}

export const SelectableTag = ({tag, index, select}) => {
  
  let {name, id} = tag;
  let deleteTagFxn = () => {
    ModalRegistrar.register(deleteTag);
    store.dispatch(setupModal({
      type: 'confirm',
      title: 'Deleting tag!',
      message: `Deleting #${name} from all previous contacts.`,
      args: [tag]
    }));
  };

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onLongPress={(!select) ? deleteTagFxn : () => {}} 
      onPress={() => { (select) ? select(name) : store.dispatch(selectTag(id))}}
      style={{ height: 'auto', marginLeft: 5, marginTop: 5}}
    >
        <RoundedTag color={getColor(name)} key={index} width='auto'>
          <TagText>{name}</TagText>
        </RoundedTag>    
    </TouchableOpacity>
  )
}


export const SmartTag = ({tag, index, select}) => {

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={() => { select(tag) }}
      style={{ height: 'auto'}}
    >
        <RoundedTag color={getColor(tag)} key={index} width='auto'>
          <TagText>{tag}</TagText>
        </RoundedTag>    
    </TouchableOpacity>
  )
}

export const Tag = ({name, index, width, count, id}) => {
  return (
    <TouchableOpacity onPress={() => { store.dispatch(selectTag(id))}} activeOpacity={0.5}>
      <RoundedTag color={getColor(name)} key={index} width={width}>
        { (count > 0) && <TagText style={{ flex: 1, marginRight: 5, fontWeight: 'bold', fontFamily: 'Roboto'}}> {count}</TagText>}
        <TagText>{name}</TagText>
      </RoundedTag>
    </TouchableOpacity>
  );
};

export const DisplayTag = ({tag, index, deselect}) => {
  return (
    <TouchableOpacity onPress={() => deselect(tag)} activeOpacity={0.5}>
      <RoundedTag color={getColor(tag)} key={index} width='auto'>
        <TagText>{tag}</TagText>
      </RoundedTag>
    </TouchableOpacity>
  )
}

const ContactTagText = styled.Text`
  fontSize: 12px;
  color: darkslategray;
`
const TagText = styled.Text`
  fontSize: 16px;
  color: darkslategray;

`

const RoundedTag = styled.View`
  maxHeight: 40px;
  paddingLeft: 5px; 
  paddingRight: 5px;
  elevation: 2;
  borderRadius: 5px;
  backgroundColor: ${(props) => (props.color)};
  alignSelf: flex-start;
  width: ${(props) => (props.width)};
  borderColor: black;
  borderWidth: 1px;
  color: white;
  justifyContent: center;
  alignItems: center;
  flexDirection: row;
`;