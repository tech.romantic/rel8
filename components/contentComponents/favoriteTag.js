import React from 'react';
import styled from 'styled-components';
import {FlatList, Text, StyleSheet, View} from 'react-native';
import randomColor from 'randomcolor';
import { Tag, getColor, getDarkColor } from './tag';
import compColors from 'complementary-colors';
import { TouchableRipple } from 'react-native-paper';
import { selectTag } from '../../redux/tags';
import store from '../../redux/store';
import { whiteDay, blackNight, nightToDay, nightShades } from '../../style/color';
import Ionicon from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import AntDesign  from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import convert from 'color-convert';
import pSBC from 'shade-blend-color';

export const FavoriteTag = ({tag, index}) => {

    let baseColor = getColor(tag.name);
    let accentRgba = new compColors(baseColor).square();
    let accentColor = convert.rgb.hex(accentRgba[3].r, accentRgba[3].b, accentRgba[3].g);

    return (
      <TouchableRipple
          style={{flex: 1, borderRadius: 5, margin: 5}}
          onPress={() => { store.dispatch(selectTag(tag.id))}}
          rippleColor={baseColor}
      >
        <LinearGradient
          colors={[baseColor, `#${accentColor}`]}
          start={{x: 0.0, y: 1.0}}
          end={{x: 1.0, y: 0.0}}
          locations={[0.5, 1.0]}
          style={{borderRadius: 5}}
        >
          <FavoriteTagCard tag={tag}
              onPress={() => { store.dispatch(selectTag(tag.id))}}
          >
          </FavoriteTagCard>
        </LinearGradient>

      </TouchableRipple>
    )
}

const FavoriteTagCard = ({tag}) => {
    let {name, index, width, contacts, messagesSent} = tag; 
    let baseColor = getColor(name);

    return ( 
        <Card color={nightShades["3"]} key={index} width={width}>
          <TagTextContainer>
          <TagText color={baseColor}>{name}</TagText>
          </TagTextContainer>
            <FavoriteStatsContainer>
                <StatsRow>
                    <SimpleLineIcons
                        name="people"
                        color={baseColor}
                        size={20}
                        style={{height: 'auto'}}
                    />
                    <StatsText color={baseColor}>{contacts.length}</StatsText>
                </StatsRow>
                <StatsRow>
                    <MaterialCommunityIcons
                            name="message-text"
                            color={baseColor}
                            size={20}
                            style={{height: 'auto'}}
                        />
                    <StatsText color={baseColor}>{messagesSent}</StatsText>
                </StatsRow>
            </FavoriteStatsContainer>
        </Card>
    );
  };

const FavoriteStatsContainer = styled.View`
  width: auto;
  paddingRight: 10px;
  alignItems: flex-end; 
  justifyContent: center;
`

const StatsRow = styled.View`
  flexDirection: row;
  justifyContent: center; 
  alignItems: flex-end;
`

const TagTextContainer = styled.View`
  flex: 1;
  alignItems: flex-start;
  justifyContent: center; 
`

const TagText = styled.Text`
  fontFamily: Indigo Regular;
  alignItems: center;
  fontSize: 24px;
  color: ${(props) => props.color || whiteDay};
  fontWeight: bold;
  paddingLeft: 10px;
`

const StatsText = styled.Text`
  fontSize: 20px;
  color: ${(props) => props.color}; 
  fontWeight: bold; 
  fontFamily: 'Roboto';
  marginLeft: 5px;
`

const Card = styled.View`
  margin:4px;
  flex: 1;
  flexDirection: row;
  borderRadius: 5px;
  backgroundColor: ${(props) => props.color};
  borderColor: ${(props) => props.borderColor || 'transparent'};
  borderWidth: 0;
  elevation: 5;
  color: ${whiteDay};
  minHeight: 100px; 
`