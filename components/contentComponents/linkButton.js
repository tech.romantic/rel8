import React from 'react';
import AwesomeButton from "react-native-really-awesome-button";
import { connect } from 'react-redux';
import styled from 'styled-components';
import justThe8 from '../../assets/images/rel8retrofinalwhite.png';
import { clearSelectedContacts } from '../../redux/contacts';
import store from '../../redux/store';
import { clearSelectedTags } from '../../redux/tags';
import { hotRed, nightToDay, trueBlue, whiteDay } from '../../style/color';
import { associateContactsWithTags } from '../../utils/associationService';

const LinkButton = ({contacts, tags}) => {
    const numberOfContactsToLink = Object.values(contacts).filter((selected) => selected).length; 
    const numberOfTagsSelected = Object.values(tags).filter((selected) => selected).length;
    const readyForAssociation = (numberOfContactsToLink > 0 && numberOfTagsSelected > 0);

    const startAssociation = async (contactsMap, tagsMap, next) => {
        // Should await a promise all from the association service... 
        await associateContactsWithTags(contactsMap, tagsMap);
        // Don't clear selected tags so its easier to send a msg immediately. 
        store.dispatch(clearSelectedTags());
        store.dispatch(clearSelectedContacts());
        // finish awesome button loading
        next();
    }

    return (
		<LinkButtonOverlay>
                <AwesomeButton
                    progress
                    backgroundColor={hotRed}
                    backgroundDarker={nightToDay["1"]}
                    backgroundDarker={nightToDay["0"]}
                    backgroundProgress={whiteDay}
                    label={numberOfContactsToLink.toString()}
                    disabled={!readyForAssociation}
                    onPress={(next) => startAssociation(contacts, tags, next)}
                    borderRadius={75}
                    width={75}
                    height={75}    
                    activityColor={trueBlue}
                    activeOpacity={0.5}
                    springRelease
                    style={{
                        opacity: (readyForAssociation) ? 1.0 : 0.5
                    }}
                >
                    <ImgEight
                    resizeMode="contain"
                        source={justThe8} 
                    />   
                </AwesomeButton>
		</LinkButtonOverlay>
	);
}

const ImgEight = styled.Image`
  marginRight: 5px;
  height: 60px;
  width: 60px;
`;

const LinkButtonOverlay = styled.View`
   position: absolute;
   right: 32px;
   bottom: 64px;
`;

const mapStateToProps = (state) => {
    return {
      contacts: state.contacts.selectedContacts,
      tags: state.tags.selectedTags,
    }
}
  
export default connect(mapStateToProps, null)(LinkButton);
