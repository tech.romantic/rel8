import randomColor from 'randomcolor';
import React, { useEffect, useState } from 'react';
import { FlatList, KeyboardAvoidingView, View } from 'react-native';
import { Avatar, CheckBox } from 'react-native-elements';
import { ActivityIndicator } from 'react-native-paper';
import Toast from 'react-native-simple-toast';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { queryRealm } from '../../realm/realm';
import { setupModal } from '../../redux/app';
import { setSelectedContacts } from "../../redux/contacts";
import store from '../../redux/store';
import { blackNight, hotRed, whiteDay } from '../../style/color';
import { deassociateTagFromContact } from '../../utils/contactsService';
import ModalRegistrar from '../../utils/modalService';
import { CenteredRow, CenteredView } from '../layoutComponents';
import { LargeText } from '../textComponents';
import { AttachedTag } from './tag';
const searchContacts = async (search) => {
  return await queryRealm('Contact', filterString(search), 0, 50);
}

const filterString = (search) => (search.length != 0 ) ? `displayName BEGINSWITH[c]'${search}'` : '';

const sortByLastTexted = [['lastTexted', true]];
const sortByRecentlyAdded = [['id', true]];
const sortAlphabetical = `displayName`;

let sortOptions = {
  "lastTexted":sortByLastTexted,
  "lastAdded": sortByRecentlyAdded,
  "alphabetical":sortAlphabetical
}

const ContactsList = ({searchQuery, selectedContacts, selectedTags, modal, sortedBy, disabled }) => {

  const [list, setList] = useState([]);
  const [offset, setOffset] = useState(0);
  const [listLoading, setListLoading] = useState(true);
  const numberOfContactsInChunk = 100;
  const isContactSelected = (selectedContacts, id) => (selectedContacts.hasOwnProperty(id) && selectedContacts[id]) 
  
  const populateContacts = async () => {
    var firstContacts = await queryRealm('Contact', filterString(searchQuery));
    var sortedContacts = firstContacts.sorted(sortOptions[sortedBy]);
    var firstSlice = sortedContacts.slice(0, numberOfContactsInChunk);
    replaceContactList(firstSlice);
  }

  useEffect(() => {
    populateContacts();
  }, [modal, selectedContacts])

  const onContactSelect = (contactId) => {
    const newSelectedContacts = {
      ...selectedContacts
    };
    newSelectedContacts[contactId] = !(isContactSelected(selectedContacts, contactId));
    store.dispatch(setSelectedContacts(newSelectedContacts));
  }

  useEffect(() => {
    setListLoading(true);
    populateContacts();
    setListLoading(false);
  }, [searchQuery, sortedBy, disabled]);

  function replaceContactList(contacts) {
    setList(contacts);
    setOffset(contacts.length);
    setListLoading(false);
  }

  return (
    <KeyboardAvoidingView
      style={{
        backgroundColor: `${blackNight}`,
        flex: 1
      }}
    >
      <FlatList
        data={list}
        contentContainerStyle={{ flexGrow: 1}}
        renderItem={({item, index}) => <ContactListItem selected={!!isContactSelected(selectedContacts, item.id)} contact={item} index={index} select={onContactSelect} />}
        extraData={[selectedContacts]}
        keyExtractor={item => item.id.toString()}
        onEndReachedThreshold={2}
        onEndReached={async ({distanceFromEnd}) => {
          if (!listLoading) {
            setListLoading(true);
            setOffset(offset+50);
            var contacts = await queryRealm('Contact', filterString(searchQuery));
            let sortedContacts =  contacts.sorted(sortOptions[sortedBy])
            let nextChunk = sortedContacts.slice(0, offset+numberOfContactsInChunk);
            replaceContactList(nextChunk);
          }
        }}
        getItemLayout={(data,index) => ({
          length: 50,
          offset: 50 * index,
          index
        })}
        ListFooterComponent={(<ListLoader message="Loading Contacts..."/>)}
        ListEmptyComponent={(!listLoading) && (<CenteredView style={{ backgroundColor: `${blackNight}`}}>
        <LargeText>No contacts found!</LargeText>
      </CenteredView>)}
        scrollEventThrottle={16}
        removeClippedSubviews={true}
        maxToRenderPerBatch={numberOfContactsInChunk}
        updateCellsBatchingPeriod={10}
        initialNumToRender={numberOfContactsInChunk}
        style={{
          zIndex: 1000,
          marginTop: 0,
        }}
      />
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = (state) => {
  return {
    searchQuery: state.contacts.search,
    selectedContacts: state.contacts.selectedContacts,
    selectedTags: state.tags.selectedTags,
    sortedBy: state.contacts.sortedBy,
    disabled: state.contacts.disabled,
    modal: state.app.modal.visible,
  }
}

export default connect(mapStateToProps, null)(ContactsList);

class ContactListItem extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  onTagPress = async (tag) => {
    let {id, displayName} = this.props.contact;
    ModalRegistrar.register(deassociateTagFromContact);
    store.dispatch(setupModal({
      type: 'confirm',
      title: 'Removing tag!',
      message: `Removing #${tag} from ${displayName}...`,
      args: [id, tag]
    }));
  }

  render() {
    const { select, contact, index, selected } = this.props;
    const {displayName, id, tags, smartTags, blocked } = contact;

    return (
      <>
        <ListItemRoot>
          <AvatarColumn>
            <ContactAvatar name={displayName}/>

          </AvatarColumn>
          <MainColumn>
            <View>
              <DisplayName>
                  {displayName}
                </DisplayName>
                <ScrollableTagsRow tags={tags} onTagPress={this.onTagPress}/>
            </View>
          </MainColumn>
          <SelectColumn>
            <CheckBox
              center
              iconType='material'
              checkedIcon='check'
              uncheckedIcon={(!blocked) ? 'check-box-outline-blank' : 'block'}
              checkedColor={whiteDay}
              checked={selected}
              onPress={(!blocked) ? (() => select(id)) : (() => Toast.show('This contact has blocked future rel8 messages!'))}
            /> 
          </SelectColumn>
        </ListItemRoot>
        </>
    );
  }
}

const ScrollableTagsRow = ({tags, onTagPress}) => {
  return (
    <ScrollableRow
      horizontal={true}
    >
      <ContactTagsRow>
        {tags.map((tag, index) => <AttachedTag onLongPress={onTagPress} key={tag.name.concat(index)} name={tag.name} />)}
      </ContactTagsRow>
    </ScrollableRow>
  )
}

const ListItemRoot = styled.View`
  flexDirection: row;
  backgroundColor: ${blackNight};
  height: 75px;
  borderBottomWidth: 1px;
`

const AvatarColumn = styled.View`
  width: 75px; 
  justifyContent: center;
  alignItems: center;
`

const MainColumn = styled.View`
  flex: 1; 
  justifyContent: center; 
  alignItems: flex-start; 
  flexDirection: column; 
`

const SelectColumn = styled.View`
  width: 75px; 
  justifyContent: center;
  alignItems: center;
`

const DisplayName = styled.Text`
  color: ${whiteDay};
  fontSize: 16px;
`

const ScrollableRow = styled.ScrollView`
  height: auto;
  flexGrow: 0;
`
const ContactTagsRow = styled.View`
  marginTop: 5px;
  flexDirection: row;
`

const ContactAvatar = ({name}) => {
  const initials = getInitials(name); 
  const color = randomColor({
    seed: name,
    luminosity: 'dark'
  });
  return (
    <Avatar
      rounded
      title={initials}
      overlayContainerStyle={{backgroundColor: color}}
    />
  )
  
}

var getInitials = function (string) {
  var names = string.split(' '),
      initials = names[0].substring(0, 1).toUpperCase();
  
  if (names.length > 1) {
      initials += names[names.length - 1].substring(0, 1).toUpperCase();
  }
  return initials;
};

export const ListLoader = ({message}) => {
  return (
    <CenteredRow style={{ backgroundColor: `${blackNight}`, color: `${whiteDay}`, height: 50, width: '100%' }}>
    <ActivityIndicator animating={true} color={hotRed} />

    </CenteredRow>
    )
  
}