import React, { useEffect, useState } from 'react';
import { Text } from 'react-native';
import { Divider } from 'react-native-paper';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import styled from 'styled-components';
// import { loadContactsFromRealm } from '../utils/contactsService';
// import ContactsList from '../components/contentComponents/contactsList';
// import { startLoading, endLoading } from '../redux/app';
import { queryRealm } from '../../realm/realm';
import { clearSelectedContacts } from '../../redux/contacts';
import store from '../../redux/store';
import { clearSelectedTags, setSelectedTags } from '../../redux/tags';
import { blackNight, hotRed, trueBlue, whiteDay } from '../../style/color';
import { Tag } from './tag';
const TagMultiSelect = ({selectedTags, relative, displayOnly, tagsUpdated}) => {   

    let [tagsList,setTagsList] = useState([]);
    let [selectedTagList, setSelectedTagList] = useState(selectedTags);
    var multiSelectRef; 
    
    async function populateTags() {
        var tags = await queryRealm('Tag');
        setTagsList(tags);
    }

    useEffect(() => {
        populateTags();
    }, [tagsUpdated]);
    
    useEffect(() => {
        setSelectedTagList(selectedTags);
    }, [selectedTags, tagsUpdated])

    return (
        <RootMultiSelect>
            { displayOnly == undefined && (
                <SelectedTagsContainer
                    relative={relative}
                > 
                    <ClearTagsContainer>
                        <MaterialIcons name="clear" size={25} color={(selectedTagList.length > 0) ? hotRed : whiteDay}
                            onPress={() => {
                                store.dispatch(clearSelectedTags()); 
                                store.dispatch(clearSelectedContacts())
                            }}
                        ></MaterialIcons>
                    </ClearTagsContainer>
                    <TagListContainer>
                        <SectionedMultiSelect
                            ref={SectionedMultiSelect => multiSelectRef = SectionedMultiSelect}
                            noResultsComponent={() => <Text style={{textAlign: 'center'}}> No tags found! Create a custom tag instead. </Text>}
                            showChips={false}
                            items={Array.from(tagsList)}
                            hideSubmitButton
                            hideDropdown
                            uniqueKey="id"
                            displayKey="name"
                            ref={(component) => component}
                            selectedItems={selectedTags}
                            onSelectedItemsChange={(tagIdsSelected) => {
                                store.dispatch(setSelectedTags(tagIdsSelected));
                            }}
                            renderSelectText={() => (selectedTags.length) > 0 ? 'Select a contact to tag' : "Select a tag by tapping it"}
                            alwaysShowSelectText={true}
                            searchInputPlaceholderText="Search tags..."
                            searchPlaceholderText="Search tags..."
                            colors={{
                                selectToggleTextColor: `${whiteDay}`,
                            }}
                            style={{
                                height: 'auto'
                            }}
                        >
                        </SectionedMultiSelect>
                    </TagListContainer>
                </SelectedTagsContainer>
            )}

            <Divider />
            { (selectedTags.length > 0 && tagsList.length > 0) && (
                <HorizontalList
                    horizontal={true}
                    contentContainerStyle={{
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                    >
                    { (displayOnly != undefined) && (<SendingToLabel> to </SendingToLabel>)}
                    { selectedTags.map((tagId, index) => {
                        let foundTag = tagsList.find((tag) => tag.id === tagId);
                        return (foundTag) 
                            ? (<Tag name={foundTag.name} count={foundTag.contacts.length} id={tagId} key={index} width="auto"/>) 
                            : null;
                    })}
                </HorizontalList>
            ) }
            { ((displayOnly) &&  selectedTags.length == 0) && <SendingToLabel>Select a tag...</SendingToLabel> }
            <Divider/>

        </RootMultiSelect>
    );
  
}

const mapStateToProps = (state) => {
    return {
      selectedTags: state.tags.selectedTags,
      tagsUpdated: state.tags.tagsUpdated
    }
  }
  
export default connect(mapStateToProps, null)(TagMultiSelect);

const RootMultiSelect = styled.View`
  flexDirection: column; 
  backgroundColor: ${blackNight};
  overflow: hidden; 
  borderWidth: 0;
`
const SelectedTagsContainer = styled.View`
    flexDirection: row;
    position: relative;
    width: 100%;
    zIndex: 500;
    elevation: 4;
    backgroundColor: ${trueBlue};
`

const ClearTagsContainer = styled.View`
  flex: 1; 
  alignItems: center;
  justifyContent: center;
`

const SendingToLabel = styled.Text`
  color: ${whiteDay};
  paddingTop: 2px;
  paddingBottom: 2px; 
  paddingLeft: 2px;
`
const HorizontalList = styled.ScrollView`
    paddingTop: 2px;
    paddingBottom: 2px; 
    paddingLeft: 2px;
    height: auto;
    width: 100%;
    background: ${blackNight};
    borderWidth: 0;
`

const TagListContainer = styled.View`
  flex: 8;
`