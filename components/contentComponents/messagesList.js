import React, {useState, useEffect, useCallback, useRef} from 'react';
import styled from 'styled-components';
import {FlatList, Text, View, ScrollView, Image, TouchableOpacity} from 'react-native';
import { queryRealm } from '../../realm/realm';
import { HorizontalSplitRow, CenteredRow, CenteredView } from '../layoutComponents';
import {AttachedTag} from './tag';
import {ActivityIndicator, Colors} from 'react-native-paper';
import store from '../../redux/store';
import { blackNight, whiteDay } from '../../style/color';
import { LargeText } from '../textComponents';
import { ListLoader } from './contactsList';
import ModalRegistrar from '../../utils/modalService';
import { setupModal } from '../../redux/app';


export const MessagesList = ({messageRefresh}) => {

    const [messages, setMessages] = useState([]);
    const [listLoading, setListLoading] = useState(true);
    let flatlist = useRef();
    const populateMessages = async () => {
        setListLoading(true);
        const messages = await queryRealm('Message');

        setMessages(messages);
        setListLoading(false);
    }

    useEffect(() => {
        populateMessages();
        flatlist.scrollToOffset({offset: Math.pow(10, 100), animated: true});
    }, [messageRefresh]);

    return (
        <FlatList
            data={messages}
            contentContainerStyle={[{flexGrow: 1},(messages.length > 1) ? { justifyContent: 'flex-end'} : { justifyContent: 'center'}]}
            renderItem={({item, index}) => <MessageItem item={item} index={index}></MessageItem>}
            keyExtractor={item => item.id.toString()}
            getItemLayout={(data,index) => ({
                length: 50,
                offset: 50 * index,
                index
            })}
            ListEmptyComponent={(<CenteredView style={{ backgroundColor: `${blackNight}`}}>
                { listLoading 
                    ? (<ListLoader message="Loading messages..."></ListLoader>)
                    : (<LargeText color={whiteDay}>Sent messages will appear here! </LargeText>)}
            </CenteredView>)}
            removeClippedSubviews={true}
            ref={ref => flatlist = ref}
      />
    )
} 


const MessageItem = ({item, index}) => {
    let {dateSent, text, sentToContactIds, sentToTagNames, imageUrl} = item;
    // Date right aligned
    // Message card 
    return (
        <MessageContainer>
            <ScrollableTagsRow index={index} tags={sentToTagNames}/>
            <MessageCard {...{dateSent, text, imageUrl}}>

            </MessageCard>
        </MessageContainer>
    )
}

const ScrollableTagsRow = ({tags}) => {
    return (
      <ScrollableRow
        horizontal={true}
      >
        <ContactTagsRow>
          {tags.map((tag, index) => <AttachedTag key={tag.concat(index)} name={tag} />)}
        </ContactTagsRow>
      </ScrollableRow>
    )
  }
  

const MessageContainer = styled.View`
    marginTop: 20px;
    height: auto;
    width: auto;
    alignItems: flex-end;
`
const MessageCard = ({dateSent, text, imageUrl}) => {

    const openImageModal = () => {
        store.dispatch(setupModal({
          type: 'image',
          args: [imageUrl]
        }));
    }
    return (
        <MRow>
            <MCardContainer>
                <MCard>
                    { (imageUrl) && (
                        
                        <MImageContainer>
                            <TouchableOpacity onPress={openImageModal}>
                                            <MImage source={{uri: imageUrl}}/>
                            </TouchableOpacity>
                        </MImageContainer>
              
                    )}
                    
                    <MContentContainer>
                        <DateSentLine>{dateSent}</DateSentLine>
                        <MessageLine>{text}</MessageLine>
                    </MContentContainer>
                </MCard>
            </MCardContainer>
        </MRow>
    )
} 

const MRow = styled.View`
    flexDirection: row; 
    alignItems: center;
    justifyContent: space-between;
    height: auto;
`
const MImageContainer = styled.View`
    width: 50px;
    marginRight: 5px;
    justifyContent: flex-end;
`;

const MImage = styled.Image`
    width: 50px;
    height: 50px;
`

const MCardContainer = styled.View`
    flex: 1;
    alignItems: flex-end;
`;

const MCard = styled.View`
    flexDirection: row;
    flexWrap: wrap; 
    paddingLeft: 10px;
    paddingRight: 10px;
    paddingTop: 5px;
    paddingBottom: 5px;
    backgroundColor: ${whiteDay};
    borderTopEndRadius: 2px;
    borderTopStartRadius: 2px;
    borderBottomStartRadius: 2px;
    elevation: 1; 
    justifyContent: flex-end; 
    alignItems: center; 
    flex: 1;
`

const MContentContainer = styled.View`
`;

const DateSentLine = styled.Text`
    color: slategrey; 
    textAlign: right;
    height: auto;
    fontSize: 10px;
`

const MessageLine = styled.Text`
    width: auto;
    textAlign: right;
    flexWrap: wrap;
    fontSize: 16px;
    paddingBottom: 5px;
`

const ScrollableRow = styled.ScrollView`
  height: auto;
  flexGrow: 0;
  marginBottom: 2px;
  marginRight: 2px;
`
const ContactTagsRow = styled.View`
  marginTop: 5px;
  flexDirection: row;
`