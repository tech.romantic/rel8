import React, { useEffect, useState } from 'react';
import { TouchableOpacity } from 'react-native';
import { Divider } from 'react-native-elements';
import Toast from 'react-native-simple-toast';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { queryRealm } from '../../realm/realm';
import { nightToDay, whiteDay } from '../../style/color';
import { queryOrCreateTagByName } from '../../utils/tagsService';
import { LargeLeftText } from '../textComponents';
import { SelectableTag } from './tag';

const YourTagsList = ({tagsUpdated, select}) => {
  let [tagsList,setTagsList] = useState([]);
  let [addMode, setAddMode] = useState(false);
  let [newTag, setNewTag] = useState('');

  const addCustomTag = async (customTagName) => {
    if (customTagName.length == 0) {
      Toast.show(`Type in a tag!`);
      return;
    }
    let tag = await queryOrCreateTagByName(customTagName);
    setNewTag('');
    setAddMode(false);
    Toast.show(`Successfully added tag ${customTagName}`);
    populateTags();
  }

  async function populateTags() {
    var tags = await queryRealm('Tag');
    var alphabeticalTags = tags.sorted('name');
    setTagsList(alphabeticalTags);
  }

  useEffect(() => {
    populateTags();
  }, [tagsUpdated]);

  return (
		<YourTagsRoot>
			<HeadingAndAdded
				addCustomTag={addCustomTag}
				addMode={addMode}
				setAddMode={setAddMode}
				newTag={newTag}
				setNewTag={setNewTag}
			/>
			<Divider />

			<TagsScroller>
				<TagsWrapper>
					{tagsList.map((tag, index) => (
						<SelectableTag
              select={select}
							style={{marginLeft: 10, marginTop: 10}}
							tag={tag}
							key={index}
						/>
					))}
				</TagsWrapper>
			</TagsScroller>
		</YourTagsRoot>
  );
};

const HeadingAndAdded = ({addMode, setAddMode, newTag, setNewTag, addCustomTag}) => {

  return (
    <AddYourTagsSwitcher>
      { (addMode) 
        ? (<>
          <TagInput
            onChangeText={setNewTag}
            value={newTag}
            placeholder="Enter a new tag..."
            color={whiteDay}
            placeholderTextColor={whiteDay}
          />
            <TouchableOpacity
              onPress={() => addCustomTag(newTag)}
            >
              <MaterialCommunityIcons
                name="check-circle-outline"
                size={25}
                color={whiteDay}
                style={{marginLeft: 10}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => setAddMode(false)}
            >
              <MaterialCommunityIcons
                name="close-circle-outline"
                size={25}
                color={whiteDay}
                style={{marginLeft: 10}}
              />
            </TouchableOpacity>
         </>)
        : (<> 
            <LargeLeftText size={24} color={whiteDay}>Your tags</LargeLeftText>
            <TouchableOpacity
              onPress={() => setAddMode(true)}
            >
              <MaterialIcons
                name="add-circle-outline"
                size={25}
                color={whiteDay}
                style={{marginLeft: 10}}
              />
            </TouchableOpacity>
          </>)
      }

    </AddYourTagsSwitcher>
  )
}

const TagInput = styled.TextInput`
  height: auto;
  borderWidth: 2px;
  borderColor: ${whiteDay};
  borderRadius: 5px;
  width: 80%;
  color: ${whiteDay};
`;
const AddYourTagsSwitcher = styled.KeyboardAvoidingView`
  marginTop: 10px;
  flexDirection: row;
  justifyContent: flex-start;
  alignItems: center;
`

const YourTagsRoot = styled.View`
  alignItems: flex-start;
  flex: 1;
`
const TagsWrapper = styled.View`
  backgroundColor: ${nightToDay["0"]};
  borderWidth: 1px;
  borderColor: ${nightToDay["2"]};
  paddingBottom: 10px;
  borderRadius: 10px;
  flexDirection: row;
  height: auto; 
  width: 100%;
  alignItems: flex-start;
  flexWrap: wrap; 
`
const TagsScroller = styled.ScrollView`
  width: 100%;
`

const mapStateToProps = (state) => {
  return {
    ...state.tags
  }
}

export default connect(mapStateToProps, null)(YourTagsList);

