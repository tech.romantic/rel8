import React, {useState, useEffect} from 'react';
import styled from 'styled-components';
import {Searchbar} from 'react-native-paper';
import {debounce} from 'lodash';
import store from '../../redux/store';
import { setSearchQuery, setContactSorting } from '../../redux/contacts';
import { blackNight, whiteDay, trueBlue, hotRed } from '../../style/color';
import { View, TextInput, Text } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { TouchableRipple } from 'react-native-paper';
import { connect } from 'react-redux';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  renderers
} from 'react-native-popup-menu';
let { Popover } = renderers;

const searchUpdate = (searchQuery) => {
  store.dispatch(setSearchQuery(searchQuery));
}

const debouncedSearchUpdate = debounce(searchUpdate, 500);

const ContactsSearch = ({search, sortedBy}) => {

  const [sortTypeIndex, setSortTypeIndex] = useState(0);
  const [sortMenuOpen, setSortMenuOpen] = useState(false);
  let sortOptions = ["lastTexted", "lastAdded", "alphabetical"];
  let sortIcons = ["timeline-text", "account-badge", "sort-alphabetical"];
  const setSortingMethod = (sortIndex) => {
    setSortTypeIndex(sortIndex);
    setSortMenuOpen(!sortMenuOpen);
    store.dispatch(setContactSorting(sortOptions[sortIndex]));
  }
  return (
    <SearchBarRoot>
      <SearchIconContainer>
        <MaterialCommunityIcons
            name="account-search"
            size={25}
            color={whiteDay}
        />

        
      </SearchIconContainer>
      <InputContainer
        placeholder="Search contacts"
        onChangeText={debouncedSearchUpdate}
        selectionColor={trueBlue}
        placeholderTextColor={whiteDay}
        iconColor={whiteDay}
        
      />
      <SortContainer>
      <TouchableRipple
              style={{flex: 1, borderRadius: 5, justifyContent: 'center', alignItems: 'center'}}
              onPress={() => setSortMenuOpen(!sortMenuOpen)}
              rippleColor={whiteDay}
            >
            <MaterialCommunityIcons
              name={sortIcons[sortTypeIndex]}
              size={25}
              color={whiteDay}

              />
            </TouchableRipple>
        <Menu 
          renderer={Popover} 
          rendererProps={{ placement: 'bottom' }} 
          opened={sortMenuOpen} 
          onSelect={sortTypeIndex => setSortingMethod(sortTypeIndex)}
          onBackdropPress={() => setSortMenuOpen(false)}
          
        >
          <MenuTrigger 
          > 
          </MenuTrigger>
          <MenuOptions customStyles={{optionText: {fontSize: 18}, optionsWrapper: {padding: 5}}}>
            <MenuOption value={0} text='Recently Texted' />
            <MenuOption value={1} text='Recently Added'>
            </MenuOption>
            <MenuOption value={2} text='Alphabetical' />
          </MenuOptions>
        </Menu>
      </SortContainer>
    </SearchBarRoot>
  );
};

const mapStateToProps = (state) => state.contacts;

export default connect(mapStateToProps)(ContactsSearch);


// material community icons 
// timeline-text
// sort-alphabetical
const SearchBarRoot = styled.View`
  maxHeight: 50px;
  height: 50px;
  borderRadius: 0;
  backgroundColor: ${hotRed};
  flexDirection: row;
`;

const InputContainer = styled.TextInput`
  flex: 4; 
  borderRadius: 0;
  backgroundColor: ${hotRed};
  flexDirection: row;
  color: ${whiteDay};
`;

const SortContainer = styled.View`
  flex: 1;
  borderRadius: 100px;
`;

const SearchIconContainer = styled.View`
  flex: 1;
  justifyContent: center;
  alignItems: center;
  borderRadius: 100px;
`;