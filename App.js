import React, {useState} from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {Provider} from 'react-redux';
import store from './redux/store';
import {connect} from 'react-redux';
import { rel8Startup, deleteAllRealmData } from './utils/syncService';
import NavigationWrapper from './screens/NavigationWrapper';
import { listenForOptOutMessages } from './utils/smsListener';

class App extends React.Component {
  constructor(props) {
    super(props);
    rel8Startup(); 
    listenForOptOutMessages();
  }

  render() {
    return (
      <Provider store={store}>
        <NavigationWrapper/>
      </Provider>
    );
  }
}

export default App; 