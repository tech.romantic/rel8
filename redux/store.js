import React from 'react';
import {configureStore, combineReducers} from '@reduxjs/toolkit';
import {reducer as contactsReducer} from './contacts';
import {reducer as appReducer} from './app';
import {reducer as tagsReducer} from './tags';
import {reducer as settingsReducer} from './settings';
import { composeWithDevTools } from 'redux-devtools-extension';

const combined = combineReducers({
  contacts: contactsReducer,
  app: appReducer,
  tags: tagsReducer,
  setting: settingsReducer
})

const store = configureStore({
  reducer: combined,
  middleware: composeWithDevTools,  
});

export default store;
