import { createSlice } from '@reduxjs/toolkit';

const appSlice = createSlice({
  name: 'app',
  initialState: {
    loading: false,
    loadingMessage: '',
    dbReady: false,
    disabled: false,
    currentUser: {
      id: '',
      name: '',
      email: '',
      verified: false,
      onboarded: false,
      messagesSent: 0
    },
    modal: {
      visible: false,
      title: '',
      type: '', // confirm, image 
      message: '',
      args: [],
      data: {}
    }
  },
  reducers: {
    startLoading: (state, action) => {
      state.loading = true;
      state.loadingMessage = action.payload;
    },
    endLoading: state => {
      state.loading = false;
      state.loadingMessage = '';
    },
    setupModal: (state, action) => {
      let { message, args, type, title, data } = action.payload; 
      state.modal.visible = true; 
      state.modal.title = title; 
      state.modal.type = type; 
      state.modal.message = message;
      state.modal.args = args;
      if (data) state.modal.data = data;
    },
    cleanupModal: (state) => {
      state.modal = {
        type: '',
        title: '',
        visible: false,
        message: '',
        args: []
      }
    },
    verify: (state) => {
      state.currentUser.verified = true; 
    },
    onboard: (state) => {
      state.currentUser.onboarded = true;
    },
    reOnboard: (state) => {
      state.currentUser.onboarded = false;
    },
    disable: (state) => {
      state.disabled = true;
    },
    setUser: (state, action) => {
      state.currentUser = action.payload
    },
    logout: (state) => {
      state.currentUser.verified = false;
    }
  },
});

export const {actions, reducer} = appSlice;
export const {logout, startLoading, endLoading, databaseIsReady, setupModal, cleanupModal, verify, onboard, setUser, disable, reOnboard} = actions;
