import React from 'react';
import {createSlice} from '@reduxjs/toolkit';
const contactsSlice = createSlice({
  name: 'contacts',
  initialState: {
    contactsList: false,
    listOffset: 0,
    search: '',
    sortedBy: 'lastTexted', // lastTexted, lastAdded, alphabetical
    selectedContacts: {},
    disabled: false,
  },
  reducers: {
    setSearchQuery: (state, action) => {
      state.search = action.payload;
    },
    setSelectedContacts: (state, action) => {state.selectedContacts = action.payload},
    clearSelectedContacts: (state) => {state.selectedContacts = {}},
    setContactSorting: (state, action) => {state.sortedBy = action.payload},
    setDisabled: (state) => { state.disabled = !state.disabled }
  },
});

export const {actions, reducer} = contactsSlice;
export const {setSearchQuery, setSelectedContacts, clearSelectedContacts, setContactSorting, setDisabled} = actions;
