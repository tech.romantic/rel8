import React from 'react';
import {createSlice} from '@reduxjs/toolkit';
import config from 'react-native-config';

const settingsSlice = createSlice({
  name: 'setting',
  initialState: {
    version: config.VERSION 
  },
  reducers: {

  },
});

export const {actions, reducer} = settingsSlice;
export const {} = actions;
