import React from 'react';
import {createSlice} from '@reduxjs/toolkit';
import {tagSuggestions} from '../data/tagDictionary';
import { taggedTemplateExpression } from '@babel/types';

const tagsSlice = createSlice({
  name: 'tags',
  initialState: {
    tagsList: false,
    suggestedTags: tagSuggestions,
    selectedTags: [],
    search: '',
    tagsUpdated: false, 
  },
  reducers: {
    setTags: (state, action) => {
      state.tagsList = action.payload;
    },
    addTags: (state, action) => {
      state.tagsList.push(...action.payload);
    },
    searchTags: (state, action) => {
      state.search = action.payload;
    },
    selectTag: (state, action) => {
      let selectedTagIndex = state.selectedTags.findIndex((tag) => (tag == action.payload));
      let isTagSelected = (selectedTagIndex > -1);
      isTagSelected 
        ? state.selectedTags.splice(selectedTagIndex, 1)
        : state.selectedTags.push(action.payload);
    },
    setSelectedTags: (state, action) => {
      state.selectedTags = action.payload;
    },
    clearSelectedTags: (state, action) => {
      state.selectedTags = [];
    },
    updateTags: (state) => { state.tagsUpdated = !state.tagsUpdated}
  },
});



export const {actions, reducer} = tagsSlice;
export const {setTags, addTags, searchTags, selectTag, setSelectedTags, clearSelectedTags, updateTags} = actions;
