import Realm from 'realm';
// import console = require('console');

export const ContactSchema = {
  name: 'Contact',
  primaryKey: 'id',
  properties: {
    id: 'int',
    displayName: 'string',
    thumbnailPath: 'string?',
    tags: 'Tag[]',
    smartTags: 'string[]',
    hasSmartTags: { type: 'bool', default: false },
    lastTexted: 'int?',
    messageCount: { type: 'int', default: 0 },
    optOutWarning: { type: 'bool', default: false },
    blocked: { type: 'bool', default: false }
  },
};

export const TagSchema = {
  name: 'Tag',
  primaryKey: 'id',
  properties: {
    id: 'string',
    name: 'string',
    contacts: {type: 'linkingObjects', objectType: 'Contact', property: 'tags'},
    messagesSent: {type: 'double', default: 0}
  },
};

export const MessageSchema = {
  name: 'Message',
  primaryKey: 'id',
  properties: {
    id: 'string',
    text: 'string',
    dateSent: 'string',
    sentToContactIds: 'string[]',
    sentToTagNames: 'string[]',
    imageUrl: 'string?',
    _pushed: 'bool',
  }
}

export const UserSchema = {
  name: 'User',
  primaryKey: 'id',
  properties: {
    id: 'string',
    name: 'string',
    email: 'string',
    phone: 'string?',
    verified: 'bool',
    onboarded: 'bool',
    disabled: 'bool',
    messagesSent: 'double',
    lastOptOutCheck: { type: 'int', default: 0 }
  }
}

export const AppSchemas = [ContactSchema, TagSchema, MessageSchema, UserSchema];

// Just open the Realm with our default schemas. Can update this later in complexity
// to pass in Schemas for open realm to use.
export const openRealm = async (schemas) => {
  return new Promise((resolve, reject) => {
    Realm.open({
      schema: AppSchemas,
      schemaVersion: 22
    }).then(realm => {
      resolve(realm);
    });
  });
};

export const queryRealmFirst = async (schema, filter = '') => {
  let results = await queryRealm(schema, filter, 0, 1);
  return results ? results[0] : false;
}

export const queryRealm = async (schema, filter = '') => {
  let realmDB = await openRealm([]);
  var results = realmDB.objects(schema);
  if (filter.length != 0) return results.filtered(filter);
  return results;
}

export const createObjectInRealm = async (schema, instance) => {
  let realmDB = await openRealm([]);
  let createdObject; 
  await realmDB.write(async () => {
    createdObject = await realmDB.create(schema, instance);
  });
  return createdObject;
}

export const updateObjectInRealm = async (schema, instance) => {
  let realmDB = await openRealm([]);
  let updatedObject; 
  await realmDB.write(async () => {
     updatedObject = await realmDB.create(schema, instance, 'modified');
  });
  return updatedObject;
}

export const deleteObjectInRealm = async (instance) => {
  let realmDB = await openRealm([]);
  await realmDB.write(async () => {
    realmDB.delete(instance);
  });
}

export const removeAssociationFromRealm = async (parent, parentList, childValue, propertyName) => {
  let newList = parentList.filter((item) => item[propertyName] != childValue);
  await updateObjectInRealm('Contact', {
    id: parent.id,
    tags: newList
  });

}

